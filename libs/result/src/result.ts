// from https://dev.classmethod.jp/articles/error-handling-practice-of-typescript/
export type Result<T, E> = Ok_<T, E> | Err_<T, E>;
export function Err<E>(value?: E) {
  return new Err_(value);
}
export function Ok<T>(value: T) {
  return new Ok_(value);
}

export function From<T, E>(object: Result<T, E>): Result<T, E> {
  if ((object as any).type == 'ok') {
    return new Ok_(object.value as T);
  }
  return new Err_(object.value as E);
}

export class Ok_<T, E> {
  constructor(readonly value: T) {}
  private type = 'ok' as const;
  isOk(): this is Ok_<T, E> {
    return true;
  }
  isErr(): this is Err_<T, E> {
    return false;
  }
}

export class Err_<T, E> {
  constructor(readonly value?: E) {}
  // eslint-disable-next-line:no-unused-variable
  private type = 'err' as const;
  isOk(): this is Ok_<T, E> {
    return false;
  }
  isErr(): this is Err_<T, E> {
    return true;
  }
}
