import automerge, {
  BackendState,
  BinarySyncState,
  Change,
  SyncState,
  BinarySyncMessage,
  decodeChange,
  BinaryDocument,
} from 'automerge';
const { Backend, Frontend } = automerge;

// import admin from "firebase-admin";

import { Err, Ok, Result } from 'result';

export type PeerId = string | undefined;
export type DocId = string;

export interface SyncData {
  docId: DocId;
  binarySyncMessage: BinarySyncMessage;
}

export interface SyncDataFromSocket_io {
  docId: DocId;
  binarySyncMessage: ArrayBuffer;
}

export function toSyncData(data: SyncDataFromSocket_io): SyncData {
  return {
    docId: data.docId,
    binarySyncMessage: new Uint8Array(data.binarySyncMessage) as BinarySyncMessage,
  };
}

export class AutomergeManager {
  // Instantiate a storage client
  readonly syncStates = new Map<DocId, Map<PeerId, SyncState>>();
  // a hash of [docId][source] containing in-memory sync states

  readonly backendStates = new Map<DocId, BackendState>();
  // a hash by [docId] of current backend values

  readonly persistentSyncStates = new Map<[DocId, PeerId], BinarySyncState>();
  // readonly bucket = admin.storage().bucket();

  isLoaded(docId: string): boolean {
    return this.backendStates.has(docId);
  }

  create(
    docId: DocId,
    callbackToInitialize: (docId: string) => void
  ): Result<BinaryDocument, unknown> {
    if (this.backendStates.has(docId)) return Err('Doc already exists.');

    let [, change] = Frontend.from(callbackToInitialize(docId));
    const [state] = Backend.applyLocalChange(Backend.init(), change);
    this.backendStates.set(docId, state);
    return Ok(Backend.save(state));
  }

  save(docId: DocId, state: BackendState) {
    docId || state;
    // this.bucket.file(docId).save(Buffer.from(Backend.save(state)));
  }

  load(docId: DocId, data: BinaryDocument) {
    const state = Backend.load(data);
    this.backendStates.set(docId, state);
    return state;
  }

  private setBackendState(docId: DocId, nextBackend: automerge.BackendState) {
    this.backendStates.set(docId, nextBackend);
    this.save(docId, nextBackend);
  }

  prepareSyncState(docId: DocId, peerId?: PeerId) {
    let syncStatesOfDoc = this.syncStates.get(docId);
    if (!syncStatesOfDoc) {
      syncStatesOfDoc = new Map<PeerId, SyncState>();
      this.syncStates.set(docId, syncStatesOfDoc);
    }

    const binarySyncState = this.persistentSyncStates.get([docId, peerId]);
    const syncState = binarySyncState
      ? Backend.decodeSyncState(binarySyncState)
      : Backend.initSyncState();
    syncStatesOfDoc.set(peerId, syncState);
  }

  receiveSyncMessage(
    data: SyncData,
    peerId?: PeerId
  ): [automerge.Patch | undefined, automerge.BackendState] {
    const syncStatesOfDoc = this.syncStates.get(data.docId)!;
    const [nextBackend, nextSyncState, patch] = Backend.receiveSyncMessage(
      this.backendStates.get(data.docId)!,
      syncStatesOfDoc.get(peerId) || Backend.initSyncState(),
      data.binarySyncMessage
    );
    this.setBackendState(data.docId, nextBackend);
    syncStatesOfDoc.set(peerId, nextSyncState);

    return [patch, nextBackend];
  }

  isOldChange(data: SyncData) {
    const binarySyncMessage = data.binarySyncMessage;

    const now = new Date();
    const threshold = now.setMinutes(now.getMinutes() - 5);

    const syncMessage = Backend.decodeSyncMessage(binarySyncMessage);
    const changes = syncMessage.changes.map((e) => decodeChange(e));

    return changes.some((e) => e.time < threshold);
  }

  applyLocalChange(docId: DocId, change: Change) {
    const backendState = this.backendStates.get(docId)!;
    const [changedBackendState, patch] = Backend.applyLocalChange(backendState, change);
    this.setBackendState(docId, changedBackendState);
    return patch;
  }

  updatePeers(docId: DocId, callback: (data: SyncData, peerId: PeerId | undefined) => void) {
    const backendState = this.backendStates.get(docId)!;
    const syncStatesOfDoc = this.syncStates.get(docId)!;
    syncStatesOfDoc.forEach((syncState, peerID) => {
      this.emitSyncMessage(docId, callback, peerID, backendState, syncStatesOfDoc, syncState);
    });
  }

  async emitSyncMessage(
    docId: DocId,
    callback: (data: SyncData, peerId: PeerId) => void,
    peerId?: PeerId,
    backendState = this.backendStates.get(docId)!,
    syncStatesOfDoc = this.syncStates.get(docId)!,
    syncState = syncStatesOfDoc.get(peerId)!
  ) {
    const [nextSyncState, binarySyncMessage] = Backend.generateSyncMessage(backendState, syncState);
    syncStatesOfDoc.set(peerId, nextSyncState);

    if (binarySyncMessage) {
      callback(
        {
          docId,
          binarySyncMessage,
        },
        peerId
      );
    }
  }
}
