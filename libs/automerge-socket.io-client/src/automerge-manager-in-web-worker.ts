import { From, Result } from 'result';
import { Backend, BinaryDocument, Change, Patch } from 'automerge';
import { io, Socket } from 'socket.io-client';
import {
  AutomergeManager,
  DocId,
  SyncData,
  SyncDataFromSocket_io,
  toSyncData,
} from 'automerge-manager';

export class AutomergeManagerInClientWebworker {
  readonly automergeManager = new AutomergeManager();
  socket: Socket | null = null;
  callbackToApplyPatchs = new Map<DocId, (patch: Patch) => void>();
  callbackToMessages = new Map<DocId, (message: string) => void>();
  lastDocId: string = '';

  constructor() {
    this.openConnection(undefined);
  }

  createNewDocInServer(docId: DocId) {
    this.socket?.emit('automerge/createNewDoc', { docId }, (res: Result<ArrayBuffer, string>) => {
      res = From(res);
      if (res.isOk()) {
        this.loadDocFromServer(docId, res.value);
      } else {
        this.callbackMessageToRenderingThread(docId, res.value!);
      }
    });
  }

  loadFromServer(docId: string) {
    this.lastDocId = docId;
    this.socket?.emit('automerge/load', { docId }, (res: Result<ArrayBuffer, string>) => {
      res = From(res);
      if (res.isOk()) {
        this.loadDocFromServer(docId, res.value);
      } else {
        this.callbackMessageToRenderingThread(docId, res.value!);
      }
    });
  }

  private loadDocFromServer(docId: string, bin: ArrayBuffer) {
    const state = this.automergeManager.load(docId, new Uint8Array(bin) as BinaryDocument);
    this.callbackPatchToRenderingThread(docId, Backend.getPatch(state));
    this.automergeManager.prepareSyncState(docId);
  }

  openConnection(token?: string) {
    if (!token) return;

    this.socket?.close();

    const opt = token
      ? {
          query: {
            token: token,
          },
        }
      : {};
    this.socket = io('http://localhost:8080', opt);
    this.subscribe();

    if (this.lastDocId.length) {
      this.loadFromServer(this.lastDocId);
    }
  }

  registerCallback(
    docId: DocId,
    callbackToApplyPatch: (patch: Patch) => void,
    callbackToMessage: (message: string) => void
  ) {
    this.callbackToApplyPatchs.set(docId, callbackToApplyPatch);
    this.callbackToMessages.set(docId, callbackToMessage);
    this.loadFromServer(docId);
    // const patches = this.automergeManager.initialPathes.get(docId);
    // if (patches) {
    //   this.automergeManager.initialPathes.delete(docId);
    //   for (const patch of patches) {
    //     callbackToApplyPatch(patch);
    //   }
    // }
  }

  private subscribe() {
    this.socket?.on('connect', () => {
      this.onConnect();
    });
    this.socket?.on('automerge/syncMessage', (data: SyncDataFromSocket_io) => {
      this.onSyncMessage(toSyncData(data));
    });
    this.socket?.onAny((name: string, data: SyncDataFromSocket_io) => {
      console.log(name);
      console.log(data);
    });
  }

  private onConnect() {
    console.log(this.socket?.id);
  }

  onSyncMessage(data: SyncData) {
    const [patch] = this.automergeManager.receiveSyncMessage(data);
    if (patch) {
      this.callbackPatchToRenderingThread(data.docId, patch);
    }
  }

  private callbackPatchToRenderingThread(docId: DocId, patch: Patch) {
    if (!patch) return;

    const callback = this.callbackToApplyPatchs.get(docId);
    if (callback) {
      callback(patch);
    } else {
      throw new Error();
      // this.automergeManager.initialPathes.get(docId)?.push(patch);
    }
  }

  private callbackMessageToRenderingThread(docId: DocId, message: string) {
    this.callbackToMessages.get(docId)!(message);
  }

  // init(docId: DocId, change: Change) {
  //   this.automergeManager.prepareSyncState(docId, change);
  //   this.socket?.emit("automerge/hello", { docId });
  // }

  applyLocalChange(docId: DocId, change: Change) {
    const patch = this.automergeManager.applyLocalChange(docId, change);
    this.callbackPatchToRenderingThread(docId, patch);
    this.updateServer(docId);
  }

  private async updateServer(docId: DocId) {
    this.automergeManager.emitSyncMessage(docId, (data) => {
      this.socket?.emit('automerge/syncMessage', data);
    });
  }
}

export class AutomergeManagerInClientWebworkerMock extends AutomergeManagerInClientWebworker {
  registerCallback(
    docId: DocId,
    callbackToApplyPatch: (patch: Patch) => void,
    callbackToMessage: (message: string) => void
  ) {
    (async () => {
      super.registerCallback(docId, callbackToApplyPatch, callbackToMessage);
    })();
  }

  // init(docId: DocId, change: Change) {
  //   (async () => {
  //     super.init(docId, change);
  //   })();
  // }

  applyLocalChange(docId: DocId, change: Change) {
    (async () => {
      super.applyLocalChange(docId, change);
    })();
  }
}
