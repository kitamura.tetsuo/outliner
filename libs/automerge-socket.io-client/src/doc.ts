import { ChangeFn, Doc, Frontend, Patch } from 'automerge';
import { useMemo, useState } from 'react';
import * as Comlink from 'comlink';

import { AutomergeManagerInClientWebworker } from './automerge-manager-in-web-worker';

// Add this in your component file
require('react-dom');
(window as any).React2 = require('react');
console.log((window as any).React1 === (window as any).React2);

const docs = new Map<string, Doc<any>>();
let automergeManagerInClientWebworker:
  | Comlink.Remote<AutomergeManagerInClientWebworker>
  | AutomergeManagerInClientWebworker;

export interface DocProps<T> {
  doc: Doc<T>;
  docId: string;
  setDoc: (value: Doc<T>) => void;
}

export function change<T>(props: DocProps<T>, callback: ChangeFn<T>) {
  changeSub(props, callback);
}

export function changes<T>(props: DocProps<T>, callbacks: ChangeFn<T>[]) {
  changeSub(props, (doc: T) => callbacks.forEach((e) => e(doc)));
}

function changeSub<T>(props: DocProps<T>, callback: ChangeFn<T>) {
  // const doc = docs.get(docId)!;
  const [loaclChangedDoc, change] = Frontend.change(props.doc, 'change text', callback);
  docs.set(props.docId, loaclChangedDoc);
  props.setDoc(loaclChangedDoc);

  automergeManagerInClientWebworker?.applyLocalChange(props.docId, change);
}

export function useDoc<T>(
  docId: string,
  initialDoc: Doc<T>
): [Doc<T>, string, (value: Doc<T>) => void] {
  const [doc, setDoc] = useState<Doc<T>>(initialDoc);
  const [message, setMessage] = useState('');

  useMemo(
    () => {
      if (!docs.has(docId)) docs.set(docId, doc);
      registerCallback(docId, setDoc, setMessage);
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  return [doc, message, setDoc];
}

export function registerAutomergeManagerInClientWebworker(
  remoteAutomergeManagerInClientWebworker:
    | Comlink.Remote<AutomergeManagerInClientWebworker>
    | AutomergeManagerInClientWebworker
) {
  automergeManagerInClientWebworker = remoteAutomergeManagerInClientWebworker;
}

function registerCallback<T>(
  docId: string,
  setDoc: (value: Doc<T>) => void,
  setMessage: (message: string) => void
) {
  automergeManagerInClientWebworker.registerCallback(
    docId,
    Comlink.proxy((patch: Patch) => {
      const changedDoc = Frontend.applyPatch(docs.get(docId)!, patch);
      docs.set(docId, changedDoc);
      setDoc(changedDoc);
    }),
    Comlink.proxy((message: string) => {
      setMessage(message);
    })
  );
}

export function tearDownForTest() {
  docs.clear();
}
