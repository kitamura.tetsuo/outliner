import { Result } from 'result';

export interface Storage {
  save(path: string, data: Buffer): Promise<Result<unknown, unknown>>;
  load(path: string): Promise<Result<Buffer, unknown>>;
}
