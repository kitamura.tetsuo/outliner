import { Result, Ok, Err } from 'result';
import { BinaryDocument } from 'automerge';
import express from 'express';
import { createServer as httpCreateServer } from 'http';
import { Server, Socket } from 'socket.io';

import {
  AutomergeManager,
  PeerId,
  SyncData,
  SyncDataFromSocket_io,
  toSyncData,
} from 'automerge-manager';
import { DecodedIdToken } from './decodedIdToken';
import { Storage } from './storage';
import { Auth } from './auth';

import pkg from 'automerge';
const { Backend } = pkg;

export function createServer(
  auth: Auth,
  storage: Storage,
  callbackToInitialize: (docId: string) => void
) {
  const app = express();

  const httpServer = httpCreateServer(app);
  const automergeManager = new AutomergeManager();

  app.get('/', (_, res) => {
    res.send('Hello world');
  });

  const io = new Server(httpServer, {
    cors: {
      origin: 'http://localhost:3000',
      methods: ['GET', 'POST'],
    },
  });

  // middleware
  io.use(async (socket, next) => {
    if (!socket.handshake.query.token) return next();

    let token = String(socket.handshake.query.token!);
    try {
      const res = await auth.verifyIdToken(token);
      if (res.isOk()) socket.data.decodedToken = res.value;
      return next();
    } catch (error) {
      return next(new Error('authentication error'));
    }
  });

  const emitTo = (data: SyncData, peerId: PeerId) => {
    io.to(peerId!).emit('automerge/syncMessage', data);
  };

  io.on('connection', (socket: Socket) => {
    console.log('connected');
    socket.on('automerge/createNewDoc', (data: SyncDataFromSocket_io, callback) => {
      onCreateNewDoc(socket, toSyncData(data), callback);
    });
    socket.on('automerge/load', (data: SyncDataFromSocket_io, callback) => {
      onLoad(socket, toSyncData(data), callback);
    });
    socket.on('automerge/syncMessage', (data: SyncDataFromSocket_io) => {
      onSyncMessage(socket, toSyncData(data));
    });
    socket.onAny((name: string, data: SyncDataFromSocket_io) => {
      console.log(name);
      console.log(data);
    });
  });

  httpServer.listen(8080, () => console.log('Server is running'));

  async function onLoad(
    socket: Socket,
    data: SyncData,
    // callback: (res: Result<unknown, unknown>) => void
    callback: (res: any) => void
  ) {
    const decodedToken = socket.data.decodedToken as DecodedIdToken;
    // ToDo: check authorization
    if (!decodedToken) {
      // callback(Err("No: authorization"));
      // return;
    }

    if (!automergeManager.isLoaded(data.docId)) {
      const bin = await storage.load('doc/' + data.docId);
      if (bin.isOk()) {
        automergeManager.load(data.docId, (bin.value as unknown) as BinaryDocument);
      } else {
        callback(Err('Doc not found'));
        return;
      }
    }
    const bin = Backend.save(automergeManager.backendStates.get(data.docId)!);
    console.log(bin);
    console.log(bin.buffer);
    // callback(bin);
    callback(Ok(bin));

    await automergeManager.prepareSyncState(data.docId, socket.id);
    automergeManager.emitSyncMessage(data.docId, emitTo, socket.id);
  }

  async function onCreateNewDoc(
    socket: Socket,
    data: SyncData,
    callback: (res: Result<BinaryDocument, unknown>) => void
  ) {
    const decodedToken = socket.data.decodedToken as DecodedIdToken;
    // ToDo: check authorization
    if (!decodedToken) {
      callback(Err(''));
    }

    const bin = automergeManager.create(data.docId, callbackToInitialize);
    if (bin.isOk()) {
      storage.save('doc/' + data.docId, (bin.value as unknown) as Buffer);
    }
    callback(bin);
  }

  function onSyncMessage(socket: Socket, data: SyncData) {
    // if (automergeManager.isOldChange(data)) {
    // } else {
    const [, state] = automergeManager.receiveSyncMessage(data, socket.id);
    storage.save('doc/' + data.docId, (Backend.save(state) as unknown) as Buffer);
    // }
    automergeManager.updatePeers(data.docId, emitTo);
  }

  automergeManager.create('test', callbackToInitialize);
}
