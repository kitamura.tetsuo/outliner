import { Result } from 'result';

export interface Auth {
  verifyIdToken(token: string): Promise<Result<unknown, unknown>>;
}
