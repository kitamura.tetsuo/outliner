from dateutil.rrule import rrule, MONTHLY
from datetime import datetime

start_date = datetime(2014, 12, 31)
print(list(rrule(freq=MONTHLY, count=4, dtstart=start_date)))
