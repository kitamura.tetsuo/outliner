import sys

sys.path.insert(0, '/workspace/server-or-tools/src')

import pytest
from async_asgi_testclient import TestClient

import app

TestClient.__test__ = False  # prevent PytestCollectionWarning


@pytest.mark.asyncio
async def test_app():
  client = TestClient(app.app)

  resp = await client.post("/", json={"query": "{ hello }"})
  assert resp.status_code == 200
  assert resp.json() == {"data": {"hello": "Hello world!"}}
