import collections
import platform
import pprint
from typing import NamedTuple

# Import Python wrapper for or-tools CP-SAT solver.
if "aarch64" not in platform.platform():
    from ortools.sat.python import cp_model

pp = pprint.PrettyPrinter(indent=4)


class TaskInput(NamedTuple):
    id: int
    previous_id: int
    following_id: int
    duration: int
    duration_upper_limit: int
    duration_lower_limit: int
    resources: "list[int]"
    start: int
    start_range_start: int
    start_range_end: int


class Task(NamedTuple):
    id: int
    start: int
    end: int


class ShedulingParameter(NamedTuple):
    tasks: "list[TaskInput]"


def MinimalJobshopSat(parameter: ShedulingParameter) -> list[Task]:
    pp.pprint(parameter.tasks)

    tasks: list[TaskInput] = parameter.tasks
    if len(tasks) == 0:
        return []

    # """Minimal jobshop problem."""
    dict_task = {}
    for task in tasks:
        dict_task[task.id] = task

    resources_count = 1 + max(resouce for task in tasks for resouce in task.resources)
    all_resources = range(resources_count)

    # Computes horizon dynamically as the sum of all durations.
    horizon = sum(
        max(0, task.start, task.start_range_end)
        + max(0, task.duration, task.duration_lower_limit, task.duration_upper_limit)
        for task in tasks
    )

    # Named tuple to store information about created variables.
    task_type = collections.namedtuple("task_type", "start end interval")
    # Named tuple to manipulate solution information.
    assigned_task_type = collections.namedtuple(
        "assigned_task_type", "start index duration"
    )

    # Creates job intervals and add to the corresponding resource lists.
    var_tasks = {}
    array_of_duration_vars = []
    resouce_to_intervals = collections.defaultdict(list)

    # Create the model.
    model = cp_model.CpModel()
    assumptions = []

    for task in tasks:
        suffix = "_%i" % (task.id)
        if task.start != -1:
            start_var = model.NewConstant(task.start)
        else:
            if task.start_range_start != -1:
                start_var = model.NewIntVar(
                    task.start_range_start, task.start_range_end, "start" + suffix
                )
            else:
                start_var = model.NewIntVar(0, horizon, "start" + suffix)

        end_var = model.NewIntVar(0, horizon, "end" + suffix)
        a = model.NewBoolVar("assumption" + suffix)
        assumptions.append(a)

        if task.duration != -1:
            interval_var = model.NewOptionalIntervalVar(
                start_var, task.duration, end_var, a, "interval" + suffix
            )
        else:
            duration_var = model.NewIntVar(
                task.duration_lower_limit,
                task.duration_upper_limit,
                "duration" + suffix,
            )
            array_of_duration_vars.append(duration_var)
            interval_var = model.NewOptionalIntervalVar(
                start_var, duration_var, end_var, a, "interval" + suffix
            )

        var_tasks[task.id] = task_type(
            start=start_var, end=end_var, interval=interval_var
        )

        for resouce in task.resources:
            resouce_to_intervals[resouce].append(interval_var)

    # Create and add disjunctive constraints.
    for resource in all_resources:
        model.AddNoOverlap(resouce_to_intervals[resource])

    # Precedences inside a job.
    for task in tasks:
        if task.following_id > -1:
            a = model.NewBoolVar(f"assumption_next_{task.id}_to_{task.following_id}")
            assumptions.append(a)
            model.Add(
                var_tasks[task.id].end <= var_tasks[task.following_id].start
            ).OnlyEnforceIf(a)
        if task.previous_id > -1:
            a = model.NewBoolVar(f"assumption_previous_{task.id}_to_{task.previous_id}")
            assumptions.append(a)
            model.Add(
                var_tasks[task.previous_id].end <= var_tasks[task.id].start
            ).OnlyEnforceIf(a)

    model.AddAssumptions(assumptions)

    obj_var = model.NewIntVar(0, horizon, "obj")
    # Makespan objective.
    obj_makespan_var = model.NewIntVar(0, horizon, "makespan")
    model.AddMaxEquality(
        obj_makespan_var,
        [var_tasks[task.id].end for task in tasks if task.following_id == -1],
    )

    obj_duration_total_var = model.NewIntVar(0, horizon, "duration_total")
    model.Add(sum(array_of_duration_vars) == obj_duration_total_var)

    model.Add(obj_makespan_var - obj_duration_total_var == obj_var)

    model.Minimize(obj_var)

    # Solve model.
    solver = cp_model.CpSolver()
    status = solver.Solve(model)

    assigned_tasks = []
    if status == cp_model.MODEL_INVALID:
        print("model is invalid")
    if status == cp_model.INFEASIBLE:
        print("model is infeasible")
        for var_index in solver.SufficientAssumptionsForInfeasibility():
            print(var_index, model.VarIndexToVarProto(var_index))
    if status == cp_model.OPTIMAL or status == cp_model.FEASIBLE:
        for task in tasks:
            assigned_tasks.append(
                Task(
                    id=task.id,
                    start=solver.Value(var_tasks[task.id].start),
                    end=solver.Value(var_tasks[task.id].end),
                )
            )

        # Create one list of assigned tasks per resource.
        assigned_job = []
        for task in tasks:
            assigned_job.append(
                assigned_task_type(
                    start=solver.Value(var_tasks[task.id].start),
                    index=task.id,
                    duration=solver.Value(var_tasks[task.id].end)
                    - solver.Value(var_tasks[task.id].start),
                )
            )

        # Create per resource output lines.
        output = ""
        # Sort by starting time.
        assigned_job.sort()
        sol_line_tasks = "Resource " + str(resource) + ": "
        sol_line = "       "

        for assigned_task in assigned_job:
            name = "task_%i" % (assigned_task.index)
            # Add spaces to output to align columns.
            sol_line_tasks += "%-10s" % name

            start = assigned_task.start
            duration = assigned_task.duration
            sol_tmp = "[%i,%i]" % (start, start + duration)
            # Add spaces to output to align columns.
            sol_line += "%-10s" % sol_tmp

        sol_line += "\n"
        sol_line_tasks += "\n"
        output += sol_line_tasks
        output += sol_line

        # Finally print the solution found.
        print("Optimal Schedule Length: %i" % solver.ObjectiveValue())
        print(output)

    return assigned_tasks


if __name__ == "__main__":
    tasks: list[TaskInput] = []
    tasks.append(TaskInput(0, -1, 1, [0]))
    tasks.append(TaskInput(1, -1, 1, [0]))
    tasks.append(TaskInput(2, 0, 1, [0]))
    tasks.append(TaskInput(3, -1, 1, [0]))
    MinimalJobshopSat(ShedulingParameter(tasks))
