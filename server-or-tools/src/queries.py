from ariadne import ObjectType, convert_kwargs_to_snake_case

from store import messages, users
from or_tools import or_tool_wrapper


query = ObjectType("Query")


@query.field("hello")
def resolve_hello(*_):
    return "Hello world!"


@query.field("messages")
@convert_kwargs_to_snake_case
async def resolve_messages(obj, info, user_id):
    def filter_by_userid(message):
        return message["sender_id"] == user_id or message["recipient_id"] == user_id

    user_messages = filter(filter_by_userid, messages)
    return {"success": True, "messages": user_messages}


@query.field("userId")
@convert_kwargs_to_snake_case
async def resolve_user_id(obj, info, username):
    user = users.get(username)
    if user:
        return user["user_id"]


@query.field("jobShop")
@convert_kwargs_to_snake_case
async def resolve_job_shop(obj, info, parameter):
    parameter["tasks"] = [
        or_tool_wrapper.TaskInput(**task) for task in parameter["tasks"]
    ]
    parameter = or_tool_wrapper.ShedulingParameter(**parameter)
    ret = or_tool_wrapper.MinimalJobshopSat(parameter)
    return ret
