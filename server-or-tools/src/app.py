from subscriptions import subscription
from mutations import mutation
from queries import query
from ariadne.asgi import GraphQL
from ariadne import (
    make_executable_schema,
    load_schema_from_path,
    snake_case_fallback_resolvers,
)
from starlette.middleware.cors import CORSMiddleware

type_defs = load_schema_from_path("schema.graphql")

schema = make_executable_schema(
    type_defs, query, mutation, subscription, snake_case_fallback_resolvers
)


app = CORSMiddleware(
    GraphQL(schema, debug=True),
    allow_origins=["*"],
    allow_methods=(
        "GET",
        "POST",
        "OPTIONS",
        "access-control-allow-origin",
        "authorization",
        "content-type",
    ),
)

# app = GraphQL(schema, debug=True)

if __name__ == "__main__":
    import uvicorn

    uvicorn.run(app, host="0.0.0.0", port=8000)
