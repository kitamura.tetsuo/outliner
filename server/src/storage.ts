import { Bucket } from '@google-cloud/storage';

import { Storage } from 'automerge-socket.io-server';
import { Result, Ok, Err } from 'result';

export class StorageByFirebase implements Storage {
  // interface Storage{

  constructor(private bucket: Bucket) {}

  async save(path: string, data: Buffer): Promise<Result<unknown, unknown>> {
    const file = this.bucket.file(path);

    try {
      await file.save(data);
      return Ok('');
    } catch (error) {
      return Err(error);
    }
  }

  async load(path: string): Promise<Result<Buffer, unknown>> {
    const file = this.bucket.file(path);

    try {
      const [data] = await file.download();
      return Ok(data);
    } catch (error) {
      return Err(error);
    }
  }
}
