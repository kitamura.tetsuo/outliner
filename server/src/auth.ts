import admin from 'firebase-admin';

import { Result, Ok, Err } from 'result';
import { Auth } from 'automerge-socket.io-server';

export class AuthByFirebase implements Auth {
  // interface Auth{

  constructor(private auth: admin.auth.Auth) {}

  async verifyIdToken(token: string): Promise<Result<unknown, unknown>> {
    try {
      const decodedToken = await this.auth.verifyIdToken(token);
      return Ok(decodedToken);
    } catch (error) {
      return Err(error);
    }
  }
}
