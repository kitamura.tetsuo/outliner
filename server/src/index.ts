import admin from 'firebase-admin';

import { ProjectState, ItemState } from '../../client/src/states';

import { AuthByFirebase } from './auth';
import { StorageByFirebase } from './storage';
import { createServer } from './server.export';

admin.initializeApp({
  credential: admin.credential.applicationDefault(),
  storageBucket: 'outliner-b9da8.appspot.com',
});

const bucket = admin.storage().bucket();

const auth = new AuthByFirebase(admin.auth());
const storage = new StorageByFirebase(bucket);

createServer(auth, storage, callbackToInitialize);

function callbackToInitialize(docId: string) {
  const project = new ProjectState();
  project.name = docId;
  project.name = docId;
  project.items = [];
  project.isPublic = false;

  project.items.push(new ItemState('Welcom to outline'));
  project.items[0].children.push(new ItemState('this is item'));
  project.items[0].children.push(new ItemState('this is #tag'));

  return project;
}
