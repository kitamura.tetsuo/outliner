apt install -y make ca-certificates openssl python
update-ca-certificates
# Download and install Google Cloud SDK
cd ~
wget https://dl.google.com/dl/cloudsdk/release/google-cloud-sdk.tar.gz
tar zxvf google-cloud-sdk.tar.gz && ./google-cloud-sdk/install.sh --usage-reporting=false --path-update=true
google-cloud-sdk/bin/gcloud --quiet components update
google-cloud-sdk/bin/gcloud auth activate-service-account --key-file /secrets/outliner-b9da8-firebase-adminsdk.json

~/google-cloud-sdk/bin/gcloud config set project outliner-b9da8
~/google-cloud-sdk/bin/gcloud app versions list

~/google-cloud-sdk/bin/gcloud app deploy
~/google-cloud-sdk/bin/gcloud app browse