import { expose } from 'comlink';
import { AutomergeManagerInClientWebworker } from './worker.export';

export default {} as typeof Worker & { new (): Worker };

const automergeManagerInClientWebworker = new AutomergeManagerInClientWebworker();

expose({
  automergeManagerInClientWebworker,
});
