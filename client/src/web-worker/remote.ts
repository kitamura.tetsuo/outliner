import * as Comlink from 'comlink';

import { AutomergeManagerInClientWebworker } from './worker.export';

import WebWorker from './web.worker';

// import { registerAutomergeManagerInClientWebworker } from 'automerge-socket.io-client';
import { registerAutomergeManagerInClientWebworker } from '../hooks/doc';

// console.log(typeof WebWorker);

export let remoteAutomergeManagerInClientWebworker:
  | Comlink.Remote<AutomergeManagerInClientWebworker>
  | AutomergeManagerInClientWebworker;

if (typeof WebWorker !== 'object') {
  console.log('run in browser environment');
  const worker = new WebWorker();
  // //https://stackoverflow.com/questions/57717722/assigning-an-object-literal-to-a-typescript-generic-type
  // // export const remote = Comlink.wrap<StoreManagerInWebworker>(worker);
  // // console.log(remote);
  const _remote = Comlink.wrap<unknown>(worker) as any;
  remoteAutomergeManagerInClientWebworker = _remote?.automergeManagerInClientWebworker as Comlink.Remote<AutomergeManagerInClientWebworker>;
  console.log(_remote);
  console.log(_remote.automergeManagerInClientWebworker);
} else {
  console.log('run in test environment');
  remoteAutomergeManagerInClientWebworker = new AutomergeManagerInClientWebworker();
}

registerAutomergeManagerInClientWebworker(remoteAutomergeManagerInClientWebworker);
