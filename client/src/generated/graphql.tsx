import { gql } from '@apollo/client';
import * as Apollo from '@apollo/client';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
const defaultOptions = {};
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

export type Message = {
  __typename?: 'Message';
  content?: Maybe<Scalars['String']>;
  recipientId?: Maybe<Scalars['String']>;
  senderId?: Maybe<Scalars['String']>;
};

export type Mutation = {
  __typename?: 'Mutation';
  createMessage?: Maybe<CreateMessageResult>;
  createUser?: Maybe<CreateUserResult>;
};

export type MutationCreateMessageArgs = {
  content?: Maybe<Scalars['String']>;
  recipientId?: Maybe<Scalars['String']>;
  senderId?: Maybe<Scalars['String']>;
};

export type MutationCreateUserArgs = {
  username: Scalars['String'];
};

export type Query = {
  __typename?: 'Query';
  hello: Scalars['String'];
  jobShop?: Maybe<Array<Task>>;
  messages?: Maybe<MessagesResult>;
  userId?: Maybe<Scalars['String']>;
};

export type QueryJobShopArgs = {
  parameter: ShedulingParameter;
};

export type QueryMessagesArgs = {
  userId: Scalars['String'];
};

export type QueryUserIdArgs = {
  username: Scalars['String'];
};

export type ShedulingParameter = {
  tasks: Array<TaskInput>;
};

export type Subscription = {
  __typename?: 'Subscription';
  messages?: Maybe<Message>;
};

export type SubscriptionMessagesArgs = {
  userId?: Maybe<Scalars['String']>;
};

export type Task = {
  __typename?: 'Task';
  id: Scalars['Int'];
  start: Scalars['Int'];
  end: Scalars['Int'];
};

export type TaskInput = {
  duration: Scalars['Int'];
  duration_lower_limit?: Maybe<Scalars['Int']>;
  duration_upper_limit?: Maybe<Scalars['Int']>;
  id: Scalars['Int'];
  previous_id?: Maybe<Scalars['Int']>;
  following_id?: Maybe<Scalars['Int']>;
  start?: Maybe<Scalars['Int']>;
  start_range_start?: Maybe<Scalars['Int']>;
  start_range_end?: Maybe<Scalars['Int']>;
  resources: Array<Scalars['Int']>;
};

export type Tasks = {
  __typename?: 'Tasks';
  tasks?: Maybe<Array<Task>>;
};

export type User = {
  __typename?: 'User';
  userId?: Maybe<Scalars['String']>;
  username?: Maybe<Scalars['String']>;
};

export type CreateMessageResult = {
  __typename?: 'createMessageResult';
  errors?: Maybe<Array<Maybe<Scalars['String']>>>;
  message?: Maybe<Message>;
  success: Scalars['Boolean'];
};

export type CreateUserResult = {
  __typename?: 'createUserResult';
  errors?: Maybe<Array<Maybe<Scalars['String']>>>;
  success: Scalars['Boolean'];
  user?: Maybe<User>;
};

export type MessagesResult = {
  __typename?: 'messagesResult';
  errors?: Maybe<Array<Maybe<Scalars['String']>>>;
  messages?: Maybe<Array<Maybe<Message>>>;
  success: Scalars['Boolean'];
};

export type JobShopQueryVariables = Exact<{
  parameter: ShedulingParameter;
}>;

export type JobShopQuery = {
  __typename?: 'Query';
  jobShop?:
    | Array<{ __typename?: 'Task'; id: number; start: number; end: number }>
    | null
    | undefined;
};

export const JobShopDocument = gql`
  query jobShop($parameter: ShedulingParameter!) {
    jobShop(parameter: $parameter) {
      id
      start
      end
    }
  }
`;

/**
 * __useJobShopQuery__
 *
 * To run a query within a React component, call `useJobShopQuery` and pass it any options that fit your needs.
 * When your component renders, `useJobShopQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useJobShopQuery({
 *   variables: {
 *      parameter: // value for 'parameter'
 *   },
 * });
 */
export function useJobShopQuery(
  baseOptions: Apollo.QueryHookOptions<JobShopQuery, JobShopQueryVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<JobShopQuery, JobShopQueryVariables>(JobShopDocument, options);
}
export function useJobShopLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<JobShopQuery, JobShopQueryVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<JobShopQuery, JobShopQueryVariables>(JobShopDocument, options);
}
export type JobShopQueryHookResult = ReturnType<typeof useJobShopQuery>;
export type JobShopLazyQueryHookResult = ReturnType<typeof useJobShopLazyQuery>;
export type JobShopQueryResult = Apollo.QueryResult<JobShopQuery, JobShopQueryVariables>;
