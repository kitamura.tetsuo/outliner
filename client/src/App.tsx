import { useState, useEffect } from 'react';
import { User } from 'firebase/auth';
import { BrowserRouter as Router, Switch, Route, useParams, Link } from 'react-router-dom';

import { remoteAutomergeManagerInClientWebworker } from './web-worker/remote';

import { CreateProject } from './components/createProject';
import { auth } from './components/firebase';
import { PageType, Project } from './components/project';
import { Login } from './components/login';

import { SchedulePage } from 'components/scheduler/schedulePage';

export default function App() {
  const [user, setUser] = useState<User>();

  useEffect(() => {
    const unsubscribe = auth.onAuthStateChanged((user) => {
      if (user) {
        setUser(user);
        (async () =>
          remoteAutomergeManagerInClientWebworker?.openConnection(await user.getIdToken()))();
      }
    });

    return unsubscribe;
  }, []);

  return (
    <Router>
      <div>
        <Link to={'/'}>top</Link>
        <br />
        <Link to={'/timeline/project'}>Timeline</Link>
        <br />
        <Link to={'/day/project'}>Day</Link>
        <br />
      </div>
      <Switch>
        <Route exact path="/" render={() => <Home user={user} />} />
        <Route path="/login" render={() => <Login setUser={setUser} user={user} />} />
        <Route path="/crate-project" render={() => <CreateProject />} />
        <Route path="/timeline/:id" component={Timeline} />
        <Route path="/day/:id" component={Day} />
        <Route path="/schedule/:id" component={Schedule} />
        <Route path="/:id/:title" component={Outline} />
        <Route path="/:id" render={(props) => <Top {...props} user={user} />} />
      </Switch>
    </Router>
  );
}

type ProjectParams = {
  id: string;
};

type ProjectTitleParams = {
  id: string;
  title: string;
};

function Home(props: Props) {
  return (
    <>
      {!props.user ? <Link to={'/login'}>Login</Link> : null}
      {props.user ? (
        <>
          <Link to={'/project'}>project</Link>
          <br />
          <Link to={'/crate-project'}>crate-project</Link>
        </>
      ) : null}
    </>
  );
}

type Props = {
  user?: User;
};

function Day(props: Props) {
  let { id } = useParams<ProjectParams>();

  return <Project type={PageType.Day} projectId={id} key={id} {...props} />;
}

function Timeline(props: Props) {
  let { id } = useParams<ProjectParams>();

  return <Project type={PageType.Timeline} projectId={id} key={id} {...props} />;
}

function Top(props: Props) {
  let { id } = useParams<ProjectParams>();

  return <Project type={PageType.Top} projectId={id} key={id} {...props} />;
}

function Outline(props: Props) {
  let { id, title } = useParams<ProjectTitleParams>();

  return <Project type={PageType.Outline} projectId={id} key={id} title={title} {...props} />;
}

function Schedule(props: Props) {
  let { id, title } = useParams<ProjectTitleParams>();

  return <Project type={PageType.Schedule} projectId={id} key={id} title={title} {...props} />;
}
