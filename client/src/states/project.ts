import { ItemState } from './item';

export class ProjectState {
  name: string = '';
  items: ItemState[] = [];
  isPublic = false;
}
