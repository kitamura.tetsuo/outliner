import { List, Text as TText } from 'automerge';

// const isNode = (process.title !== 'browser');
// if(isNode){
import pkg from 'automerge';
const { Text } = pkg;
// }

export class ItemState {
  readonly children: List<ItemState> = [];

  isAlias: boolean = false;
  isTask: boolean = false;
  isShowChildren: boolean = false;
  isSearch: boolean = false;
  isCompleted: boolean = false;
  isShowCheckbox: boolean = false;
  id: string = '';
  text: TText = new Text('');
  parentID: number = 0;
  weight: number = 0;
  email: string = '';

  constructor(text?: string, children: List<ItemState> = []) {
    if (text) this.text = new Text(text);
    this.children = children;
  }

  static clone(src: ItemState) {
    const clone = new ItemState();
    clone.isAlias = src.isAlias;
    clone.isShowChildren = src.isShowChildren;
    clone.isSearch = src.isSearch;
    clone.isCompleted = src.isCompleted;
    clone.isShowCheckbox = src.isShowCheckbox;
    clone.id = src.id;
    clone.parentID = src.parentID;
    clone.weight = src.weight;

    clone.text = new Text(src.text.toString());
    clone.email = src.email;

    for (const child of src.children) {
      clone.children.push(ItemState.clone(child));
    }

    return clone;
  }
}
