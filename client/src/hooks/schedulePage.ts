import React from 'react';
import { Item } from 'state/item';
import { ProjectState } from 'states';
import { change } from './doc';
import { ItemProps, RootProps } from './schedule';

export function useProps(props: ItemProps) {
  const rootProps = props.rootProps;
  const item = Item.from(props);

  const onClickDeleteAllEvents = React.useCallback(() => {
    doDeleted(rootProps, item);
  }, []);

  const actions = { onClickDeleteAllEvents };
  return [actions] as const;
}

function doDeleted(rootProps: RootProps, item: Item, exec = true) {
  const changeFunc = (doc: ProjectState) => {
    item.getStateToChange(doc).children.splice(0, Infinity);
  };
  if (!exec) return changeFunc;
  change(rootProps, changeFunc);
}
