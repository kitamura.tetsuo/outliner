import { AppointmentModel } from '@devexpress/dx-react-scheduler';
import { Doc, getObjectId } from 'automerge';
import * as React from 'react';

import { Item } from 'state/item';
import { ItemState, ProjectState } from 'states';

import { change } from 'hooks/doc';

export type ItemProps = {
  rootProps: RootProps;
  item: ItemState;
  path: number[];
};

export type RootProps = {
  doc: Doc<ProjectState>;
  docId: string;
  setDoc: (value: Doc<ProjectState>) => void;
  rootItem: ItemState;
};

export function useOptimize() {
  const onClickOptimise = () => {};
  return onClickOptimise;
}

export function useItem(props: ItemProps) {
  const rootProps = props.rootProps;
  const item = Item.from(props);
  const data = (item.children as any) as AppointmentModel[];

  const [isAppointmentBeingCreated, setIsAppointmentBeingCreated] = React.useState(false);
  const [addedAppointment, setAddedAppointment] = React.useState({});

  const onCommitChanges = React.useCallback(
    ({ added, changed, deleted }) => {
      if (changed && added) {
        // doChanged(rootProps, item, changed);
        doChangedAndAdded(rootProps, item, added);
      } else if (added) {
        doAdded(rootProps, item, added);
      } else if (changed) {
        doChanged(rootProps, item, changed);
      }
      if (deleted !== undefined) {
        doDeleted(rootProps, item, deleted);
      }
      setIsAppointmentBeingCreated(false);
    },
    [item, rootProps, setIsAppointmentBeingCreated]
  );

  const onAddedAppointmentChange = React.useCallback((appointment) => {
    setAddedAppointment(appointment);
    setIsAppointmentBeingCreated(true);
  }, []);

  const actions = { onCommitChanges, onAddedAppointmentChange };
  return [item, isAppointmentBeingCreated, addedAppointment, actions] as const;
}

function doAdded(rootProps: RootProps, item: Item, added: any, exec = true) {
  const changeFunc = (doc: ProjectState) => {
    const itemInChange = item.toChange(doc);
    const addingItem = new ItemState();
    for (const key of Object.keys(added)) {
      (addingItem as any)[key] = added[key];
    }
    const i = itemInChange.getStateToChange(doc).children.push!(addingItem);
    itemInChange.getStateToChange(doc).children[i - 1].id = getObjectId(
      itemInChange.getStateToChange(doc).children[i - 1]
    );
  };
  if (!exec) return changeFunc;
  change(rootProps, changeFunc);
}

function doChanged(rootProps: RootProps, item: Item, changed: any, exec = true) {
  const changeFunc = (doc: ProjectState) => {
    const data = item.children;

    for (const appointment of data) {
      if (changed[appointment.state.id!]) {
        const changedProps = changed[appointment.state.id!];
        const itemStateToChange = appointment.getStateToChange(doc);
        for (const key of Object.keys(changedProps)) {
          // todo update to match AutoMearge Types
          (itemStateToChange as any)[key] = changedProps[key];
        }
      }
    }
  };
  if (!exec) return changeFunc;
  change(rootProps, changeFunc);
}

function doChangedAndAdded(rootProps: RootProps, item: Item, added: any, exec = true) {
  const changeFunc = (doc: ProjectState) => {
    const target = item.children.find(
      (e) => ((e.state as any) as AppointmentModel).title === added.title
    ) as Item;
    const addingItem = target.getStateToChange(doc);
    // for (const key of Object.keys(added)) {
    //   (addingItem as any)[key] = added[key];
    // }
    (addingItem as any).startDate = added.startDate;
    (addingItem as any).endDate = added.endDate;
    (addingItem as any).notes = added.notes;
  };
  if (!exec) return changeFunc;
  change(rootProps, changeFunc);
}

function doDeleted(rootProps: RootProps, item: Item, deleted: any, exec = true) {
  const changeFunc = (doc: ProjectState) => {
    const children = item.getStateToChange(doc).children;

    for (let i = 0; i < children.length; i++) {
      const element = children[i];
      if (element.id === deleted) {
        children.splice(i, 1);
      }
    }
  };
  if (!exec) return changeFunc;
  change(rootProps, changeFunc);
}
