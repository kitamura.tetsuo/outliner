import { Doc } from 'automerge';
import { useMemo, useState } from 'react';

import simpleDiff from '../util/simpleDiff';
import { Item } from '../state/item';
import { ProjectState, ItemState } from '../states';
import { TextSelection } from '../state/textSelection';

import { useHistory } from 'react-router-dom';
import { change, changes } from './doc';

export type ItemProps = {
  rootProps: RootProps;
  item: ItemState;
  path: number[];
  isTop?: boolean;
};

export type ProjectChildProps = {
  doc: Doc<ProjectState>;
  docId: string;
  setDoc: (value: Doc<ProjectState>) => void;
  title?: string;

  // history: History;
};

export type RootProps = {
  doc: Doc<ProjectState>;
  docId: string;
  setDoc: (value: Doc<ProjectState>) => void;
  rootItem: ItemState;
  pathToRefs: Map<string, React.RefObject<HTMLInputElement>>;
  setFocused: React.Dispatch<React.SetStateAction<string | null>>;
  inputSelectionPosition: React.MutableRefObject<number>;
  textSelections: TextSelection[];
  // history: History;
};

export function useInput(initialValue: string) {
  const [value, set] = useState(initialValue);
  return {
    value,
    onChange: (event: React.ChangeEvent<HTMLInputElement>) => set(event.target.value),
  };
}

export function useItem(props: ItemProps) {
  const rootProps = props.rootProps;
  const item = Item.from(props);
  const history = useHistory();

  const [text, setText] = useState(props.item.text.toString());
  useMemo(() => {
    setText(props.item.text.toString());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.item.text.toString()]);
  // const navigate = useNavigate();

  function onChanged(next: string, target: HTMLInputElement) {
    props.rootProps.inputSelectionPosition.current = target.selectionStart!;
    const diff = simpleDiff(props.item.text.toString(), next);
    if (!diff.insert && !diff.remove) return;
    if (props.isTop) {
      history.replace(
        // props.rootProps.history.replace(
        'http://localhost:3000/' + props.rootProps.docId + '/' + next,
        {
          index: item.topIndex,
        }
      );
      //     navigate.replace(indexRoutes._.test, { projectId: props.rootProps.projectId,title:next,top: {
      //   index: item.topIndex
      // } });
    }

    change(rootProps, (doc: ProjectState) => {
      const target = item.getStateToChange(doc).text;
      if (diff.insert) target.insertAt!(diff.index, diff.insert);
      if (diff.remove) target.deleteAt!(diff.index, diff.remove);
    });
  }

  const onChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setText(event.target.value);

    if (event.nativeEvent instanceof InputEvent && event.nativeEvent.isComposing) {
      return;
    }
    const lastPathKey = item.pathKey;
    onChanged(event.target.value, event.target);
    if (lastPathKey !== item.pathKey) {
      focusTo(rootProps, item);
    }
  };

  const onKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
    switch (event.key) {
      case 'Down': // IE/Edge specific value
      case 'ArrowDown':
        // Do something for "down arrow" key press.
        if (event.altKey) {
          moveDown(rootProps, false);
        } else if (event.ctrlKey) {
          moveDown(rootProps, true);
        } else {
          moveDownFocus(rootProps, item);
        }
        break;
      case 'Up': // IE/Edge specific value
      case 'ArrowUp':
        // Do something for "up arrow" key press.
        if (event.altKey) {
          moveUp(rootProps, false);
        } else if (event.ctrlKey) {
          moveUp(rootProps, true);
        } else {
          moveUpFocus(rootProps, item);
        }
        break;
      case 'Left': // IE/Edge specific value
      case 'ArrowLeft':
        // Do something for "left arrow" key press.
        rootProps.inputSelectionPosition.current = Math.max(
          0,
          event.currentTarget.selectionStart! - 1
        );
        break;
      case 'Right': // IE/Edge specific value
      case 'ArrowRight':
        // Do something for "right arrow" key press.
        rootProps.inputSelectionPosition.current = Math.min(
          event.currentTarget.value.length,
          event.currentTarget.selectionStart! + 1
        );
        break;
      case 'Tab':
        event.preventDefault();
        if (event.shiftKey) {
          outdent(rootProps, item);
        } else {
          indent(rootProps, item);
        }
        event.preventDefault();
        break;
      case 'Enter':
        event.preventDefault();
        if (event.ctrlKey && event.shiftKey) {
          createNewSiblingItem(rootProps, item, 0);
        } else if (event.ctrlKey) {
          createNewSiblingItem(rootProps, item, 1);
        } else {
          const [front, rear] = splitItemTextByCaret(event);
          if (front.length) {
            // ffssrr -> ff, rr
            // ffss   -> ff,
            change(rootProps, (doc: ProjectState) => {
              const target = item.getStateToChange(doc).text;
              target.deleteAt!(
                event.currentTarget.selectionStart!,
                target.length - event.currentTarget.selectionStart!
              );
              createNewSiblingItem(rootProps, item, 1, rear, false)!(doc);
            });
          } else if (!front.length && rear.length) {
            //   ssrr -> , rr
            change(rootProps, (doc: ProjectState) => {
              const target = item.getStateToChange(doc).text;
              createNewSiblingItem(rootProps, item, 0, rear, false)!(doc);
              if (event.currentTarget.selectionEnd! > 0)
                target.deleteAt!(0, event.currentTarget.selectionEnd!);
            });
          } else if (!front.length && !rear.length) {
            //   ss   -> ,
            change(rootProps, (doc: ProjectState) => {
              const target = item.getStateToChange(doc).text;
              if (target.length) target.deleteAt!(0, target.length);
              createNewSiblingItem(rootProps, item, 1, '', false)!(doc);
            });
          }
        }
        event.preventDefault();
        break;
      case 'Esc': // IE/Edge specific value
      case 'Escape':
        // Do something for "esc" key press.
        break;
      default:
        return; // Quit when this doesn't handle the key event.
    }
  };

  const onCompositionEnd = (event: React.CompositionEvent<HTMLInputElement>) =>
    onChanged(text, event.currentTarget);

  const onMouseUp = (event: React.MouseEvent<HTMLInputElement>) => {
    focusTo(rootProps, item, (event.target as HTMLInputElement).selectionStart!);
  };
  const actions = { onChange, onCompositionEnd, onKeyDown, onMouseUp };
  return [item, text, actions] as const;
}

function splitItemTextByCaret(
  event: React.KeyboardEvent<HTMLInputElement>
): readonly [string, string] {
  const target = event.currentTarget;
  const front = target.value.substring(0, target.selectionStart!);
  const rear = target.value.substring(target.selectionEnd!);

  return [front, rear];
}

// export function createTopItem(
//   // docId: string,
//   doc: Doc<ProjectState>,
//   setDoc: (value: Doc<ProjectState>) => void,
//   title: string
// ) {
//   changeSub(doc, setDoc, (doc: ProjectState) => {
//     doc.items.push(new ItemState(title));
//   });
// }

function createChild(rootProps: RootProps, item: Item, text?: string, exec = true) {
  const changeFunc = (doc: ProjectState) => {
    const itemInChange = item.toChange(doc);
    itemInChange.getStateToChange(doc).children.insertAt!(0, new ItemState(text));
    focusTo(rootProps, item.getChild(0), 0);
  };
  if (!exec) return changeFunc;
  change(rootProps, changeFunc);
  // should move focus to new item
}

function createNewSiblingItem(
  rootProps: RootProps,
  item: Item,
  offset: number,
  text?: string,
  exec = true
) {
  if (item.isRootItem) {
    return createChild(rootProps, item, text, exec);
  }

  const changeFunc = (doc: ProjectState) => {
    const ret = item.getParent();
    if (ret.isErr()) throw new Error();
    const parent = ret.value;

    const index = item.index + offset;
    parent.getStateToChange(doc).children.splice(index, 0, new ItemState(text));
    focusTo(rootProps, parent.getChild(index), 0);
  };
  if (!exec) return changeFunc;
  change(rootProps, changeFunc);
  // should move focus to new item
}

export function createNewChildItem(
  rootProps: RootProps,
  item: Item,
  offset: number,
  text?: string,
  exec = true
) {
  if (item.isRootItem) {
    return createChild(rootProps, item, text, exec);
  }

  const changeFunc = (doc: ProjectState) => {
    const ret = item.getParent();
    if (ret.isErr()) throw new Error();
    const parent = ret.value;

    const index = item.index + offset;
    parent.getStateToChange(doc).children.push(new ItemState(text));
    focusTo(rootProps, parent.getChild(index), 0);
  };
  if (!exec) return changeFunc;
  change(rootProps, changeFunc);
  // should move focus to new item
}

function indent(rootProps: RootProps, item: Item) {
  const index = item.index;
  if (!index) return;

  change(rootProps, (doc: ProjectState) => {
    const parent = item.getParent();
    if (parent.isErr()) throw new Error();

    const newParent = parent.value.getChild(index - 1);
    const movedIndex = move(doc, item, newParent);
    focusTo(rootProps, newParent.getChild(movedIndex));
  });
}

function outdent(rootProps: RootProps, item: Item) {
  const grandParent = item.getGrandParent();
  if (grandParent.isErr()) return;
  moveTo(rootProps, item, grandParent.value, true);
}

function moveTo(props: RootProps, item: Item, newPath: Item, doFocus = false, exec = true) {
  const changeFunc = (doc: ProjectState) => {
    item = item.toChange(doc);
    newPath = newPath.toChange(doc);

    const newParent = newPath.getParent();
    if (newParent.isErr()) throw new Error('check doc structure before call change of automerge');

    move(doc, item, newParent.value, newPath.index);
  };
  if (!exec) return changeFunc;

  change(props, changeFunc);
  if (doFocus) focusTo(props, newPath);
}

function focusTo(
  props: RootProps,
  item: Item,
  inputSelectionPosition: number | undefined = undefined
) {
  if (inputSelectionPosition !== undefined)
    props.inputSelectionPosition.current = inputSelectionPosition;
  props.textSelections.splice(
    0,
    1,
    new TextSelection(
      item.path,
      inputSelectionPosition || 0,
      item.path,
      inputSelectionPosition || 0
    )
  );
  props.setFocused(item.pathKey);
}

function move(doc: Doc<ProjectState>, item: Item, destParent: Item, destIndex: number = -1) {
  // https://github.com/automerge/automerge/issues/319
  const parent = item.getParent();
  if (parent.isErr()) throw new Error();

  const removedItems = parent.value.getStateToChange(doc).children.splice(item.index, 1);
  const deepclone = removedItems.map((e: ItemState) => ItemState.clone(e));
  if (destIndex === -1) return destParent.getStateToChange(doc).children.push(...deepclone) - 1;
  else {
    destParent.getStateToChange(doc).children.insertAt!(destIndex, ...deepclone);
    return destIndex;
  }
}

function moveUpFocus(rootProps: RootProps, item: Item) {
  const prev = item.getPrev();
  if (prev.isErr()) return;
  focusTo(rootProps, prev.value);
}

function moveDownFocus(rootProps: RootProps, item: Item) {
  const next = item.getNext();
  if (next.isErr()) return;
  focusTo(rootProps, next.value);
}

function getSiblingItems(doc: Doc<ProjectState>, selection: TextSelection) {
  const selectedItems = getSelectedItems(doc, selection);
  const startIndentLevel = selectedItems[0].indentDepth;
  if (
    selectedItems.reduce((a, b) => (a.indentDepth < b.indentDepth ? a : b)).indentDepth <
    startIndentLevel
  )
    return;

  return selectedItems.filter((e) => e.indentDepth === startIndentLevel);
}

function getSelectedItems(doc: Doc<ProjectState>, selection: TextSelection) {
  let selectedItems: Item[] = [];
  const startItem = new Item(doc, selection.startItemPath);
  const endItem = new Item(doc, selection.endItemPath);
  selectedItems.push(startItem);

  if (startItem.equal(endItem)) return selectedItems;

  let next = startItem;
  while (true) {
    const ret = next.getNext();
    if (ret.isErr()) break;
    next = ret.value;
    selectedItems.push(next!);
    if (pathEqual(next.path, endItem.path)) break;
  }

  return selectedItems;
}

function moveUp(rootProps: RootProps, moveLine: boolean) {
  const textSelections = rootProps.textSelections;
  for (const selection of textSelections) {
    moveSelectionUp(rootProps, selection, moveLine);
  }
}

function moveSelectionUp(rootProps: RootProps, selection: TextSelection, moveLine: boolean) {
  const siblingItems = getSiblingItems(rootProps.doc, selection);
  if (!siblingItems) return;

  const start = new Item(rootProps.doc, selection.startItemPath);
  let res = start.getPrev();
  if (res.isErr()) return;
  const prev = res.value;

  res = start.getParent();
  if (res.isErr()) return;

  res = prev.getParent();
  if (res.isErr()) return;
  const prevParent = res.value;

  if (moveLine) {
    moveTo(rootProps, start, prev);
  } else {
    if (prev.indentDepth <= start.indentDepth || prev.index > 0) {
      changes(
        rootProps,
        siblingItems
          .slice()
          .reverse()
          .map((e) => moveTo(rootProps, e, prev, false, false)!)
      );
    } else {
      const prevGrandParent = prevParent.getParent();
      if (prevGrandParent.isErr()) return;
      changes(
        rootProps,
        siblingItems
          .slice()
          .reverse()
          .map((e) => moveTo(rootProps, e, prevParent, false, false)!)
      );
      // for (const i of siblingItems) {
      //   // actionInUIProcess.addChiled(prev.parent?.parent?.itemState, i.itemState, prev.parent.itemState.index);
      // }
    }
  }
}
function moveDown(rootProps: RootProps, moveLine: boolean) {
  const textSelections = rootProps.textSelections;
  for (const selection of textSelections.slice().reverse()) {
    moveSelectionDown(rootProps, selection, moveLine);
  }
}

function moveSelectionDown(rootProps: RootProps, selection: TextSelection, moveLine: boolean) {
  const siblingItems = getSiblingItems(rootProps.doc, selection);
  if (!siblingItems) return;

  const start = new Item(rootProps.doc, selection.startItemPath);
  const end = new Item(rootProps.doc, selection.endItemPath);
  if (moveLine) {
    const res = end.getNext();
    if (res.isErr()) return;
    const next = res.value;
    const nextParent = next.getParent();
    const startParent = start.getParent();
    if (end.hasChild) {
      const prev = start.getPrev();
      if (prev.isErr()) return;
      const sameDepth = prev.value.getByIndentDepth(end.getChild(0).indentDepth);
      const target = sameDepth.isOk() ? sameDepth.value : prev.value;
      moveTo(rootProps, end.getChild(0), target.getSibling(1));
    } else if (next.children.length) {
      moveTo(rootProps, start, next.getChild(0));
      //        actionInUIProcess.addChild(next.itemState, startPath.itemState, 0);
    } else if (nextParent.isOk() && nextParent.value.equal(end)) {
      const startPrev = start.getPrev();
      if (startPrev.isErr()) return;
      moveTo(rootProps, startPrev.value, next.getChild(start.index));
      //        actionInUIProcess.addChild(start.getPrev()?.itemState,next.itemState,start.index);
    } else {
      if (startParent.isErr()) return;
      moveTo(rootProps, startParent.value, next.getChild(start.index));
      // actionInUIProcess.addChiled(start.parent.itemState, next.itemState, start.index);
    }
  } else {
    const res = end.getNext(true);
    if (res.isErr()) return;
    const next = res.value;
    const startParent = start.getParent();
    if (next.indentDepth === start.indentDepth) {
      if (startParent.isErr()) return;
      moveTo(rootProps, next, startParent.value.getChild(start.index));
      // actionInUIProcess.addChiled(next.parent.itemState, next.itemState, start.index);
    } else if (next.indentDepth <= start.indentDepth) {
      changes(
        rootProps,
        siblingItems
          .slice()
          .reverse()
          .map((e) => moveTo(rootProps, e, next.getChild(0), false, false)!)
      );
    } else if (start.equal(end)) {
      moveSelectionDown(rootProps, selection, true);
      // start.moveDown();
    } else {
      if (startParent.isErr()) return;
      moveTo(rootProps, next, start);
      // actionInUIProcess.addChiled(start.parent.itemState, next.itemState, start.index);
    }
  }
}

export function pathEqual(path1: number[], path2: number[]) {
  return path1.length === path2.length && path1.every((value, index) => value === path2[index]);
}
