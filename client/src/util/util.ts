export function getLast<T>(arr: T[]): readonly [T, number] {
  const index = arr.length - 1;
  return [arr[index], index] as const;
}
