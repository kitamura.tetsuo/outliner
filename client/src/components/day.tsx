import FullCalendar, { EventInput } from '@fullcalendar/react';
import timeGridPlugin from '@fullcalendar/timegrid';

import { RRule, RRuleSet, rrulestr } from 'rrule';

import { useState } from 'react';
import SplitPane from 'react-split-pane';
import { ItemState } from '../states';

import { Outline, OutlineProps } from './outline';

import { useCallback } from 'react';
import { DateSelectArg, EventApi, EventClickArg } from '@fullcalendar/react';
import dayGridPlugin from '@fullcalendar/daygrid';
import allLocales from '@fullcalendar/core/locales-all';
import interactionPlugin from '@fullcalendar/interaction';

function getTasks(tasks: ItemState[], item: ItemState) {
  if (item.isTask) {
    tasks.push(item);
  }
  item.children.forEach((e) => getTasks(tasks, e));
}

let eventId = 0;

export function Day(props: OutlineProps) {
  const [currentEvents, setCurrentEvents] = useState<EventApi[]>([]);
  const handleEvents = useCallback((events: EventApi[]) => setCurrentEvents(events), []);
  const handleDateSelect = useCallback((selectInfo: DateSelectArg) => {
    let title = prompt('title')?.trim();
    let calendarApi = selectInfo.view.calendar;
    calendarApi.unselect();
    if (title) {
      calendarApi.addEvent({
        id: String(eventId++),
        title,
        start: selectInfo.startStr,
        end: selectInfo.endStr,
        allDay: selectInfo.allDay,
      });
    }
  }, []);
  const handleEventClick = useCallback((clickInfo: EventClickArg) => {
    if (window.confirm(`delete ${clickInfo.event.title}`)) {
      clickInfo.event.remove();
    }
  }, []);
  // return (
  //   <div className="demo-app">
  //     <div className="demo-app-main">
  //       <FullCalendar
  //         plugins={[dayGridPlugin, interactionPlugin]}
  //         initialView="dayGridMonth"
  //         selectable={true}
  //         editable={true}
  //         initialEvents={INITIAL_EVENTS}
  //         locales={allLocales}
  //         locale="ja"
  //         eventsSet={handleEvents}
  //         select={handleDateSelect}
  //         eventClick={handleEventClick}
  //       />
  //     </div>
  //   </div>
  // );

  const [events, setEvents] = useState<EventInput[]>([
    {
      id: '1',
      resourceId: 'a',
      start: '2021-09-14',
      end: '2021-09-16',
      title: 'event 1',
      editable: true,
    },
    {
      id: '2',
      resourceId: 'b',
      start: '2021-09-16',
      end: '2021-09-22',
      title: 'event 2',
      editable: true,
    },
  ]);

  const tasks: ItemState[] = [];
  props.projectChildProps.doc.items.forEach((e) => getTasks(tasks, e));

  return (
    <FullCalendar
      plugins={[timeGridPlugin, interactionPlugin]}
      initialView="timeGridWeek"
      events={events}
      selectable={true}
      editable={true}
      duration={7}
      locales={allLocales}
      locale="us"
      eventsSet={handleEvents}
      select={handleDateSelect}
      eventClick={handleEventClick}
      slotDuration={'00:15:00'}
      // slotLabelInterval={15}
      // slotLabelFormat={'hh(:mm)'}
    />
  );
  // return (
  //   <SplitPane split="vertical" minSize={50} defaultSize={250}>
  //     <FullCalendar
  //       plugins={[timeGridPlugin, interactionPlugin]}
  //       initialView="timeGridWeek"
  //       events={events}
  //       selectable={true}
  //       editable={true}
  //       duration={7}
  //       locales={allLocales}
  //       locale="us"
  //       eventsSet={handleEvents}
  //       select={handleDateSelect}
  //       eventClick={handleEventClick}
  //     />
  //     <Outline {...props} />
  //   </SplitPane>
  // );
}
