import { Doc, Frontend } from 'automerge';
import { useLocation } from 'react-router-dom';

import { ItemState, ProjectState } from '../states';

// import { useDoc } from 'automerge-socket.io-client';
import { useDoc } from '../hooks/doc';

import { ProjectChildProps } from '../hooks/hook';
import { Outline } from './outline';
import { Top } from './top';
import { Day } from './day';
import { Timeline } from './timeline';
import { User } from 'firebase/auth';
import { SchedulePage } from './scheduler/schedulePage';

export const PageType = {
  Top: 0,
  Outline: 1,
  Day: 2,
  Timeline: 3,
  Schedule: 4,
} as const;
// eslint-disable-next-line
export type PageType = typeof PageType[keyof typeof PageType];

export type ProjectProps = {
  user?: User;
  project?: Doc<ProjectState>;
  projectId: string;
  type: PageType;
  title?: string;
  index?: number;
};

const emptyProject = Frontend.init<ProjectState>();

export function Project(props: ProjectProps) {
  const [project, message, setProject] = useDoc(props.projectId, props.project || emptyProject);
  const location = useLocation();
  // const history = useHistory();

  if (message) {
    return <>{message}</>;
  }

  const location_state = location.state as any;

  const projectChildProps: ProjectChildProps = {
    doc: project,
    docId: props.projectId,
    setDoc: setProject,
    title: props.title,
  };
  switch (props.type) {
    case PageType.Top:
      return <Top {...projectChildProps} />;
    case PageType.Outline: {
      if (!project.items) return <>waiting</>;
      const index =
        props.index !== undefined && props.index >= 0
          ? props.index
          : location_state?.index ||
            project.items.findIndex((e: ItemState) => e.text.toString() === props.title);
      const item = project.items[index];
      return <Outline projectChildProps={projectChildProps} item={item} index={index} />;
    }
    case PageType.Day: {
      if (!project.items) return <>waiting</>;
      var d = new Date();
      const title =
        props.title ||
        `${d.getFullYear()}-${(d.getMonth() + 1)
          .toString()
          .padStart(2, '0')}-${d.getDate().toString().padStart(2, '0')}`;

      const index =
        props.index !== undefined && props.index >= 0
          ? props.index
          : location_state?.index ||
            project.items.findIndex((e: ItemState) => e.text.toString() === title);
      const item = project.items[index];
      return <Day projectChildProps={projectChildProps} item={item} index={index} />;
    }
    case PageType.Timeline: {
      if (!project.items) return <>waiting</>;
      return <Timeline {...projectChildProps} />;
    }
    case PageType.Schedule: {
      if (!project.items) return <>waiting</>;
      return <SchedulePage {...projectChildProps} />;
    }
  }
}
