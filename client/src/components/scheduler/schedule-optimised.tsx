import * as React from 'react';

import { AppointmentModel, SchedulerDateTime, ViewState } from '@devexpress/dx-react-scheduler';
import {
  Appointments,
  AppointmentTooltip,
  DateNavigator,
  Scheduler,
  TodayButton,
  Toolbar,
  WeekView,
} from '@devexpress/dx-react-scheduler-material-ui';
import * as yaml from 'js-yaml';
import { rrulestr } from 'rrule';

import { ItemState } from 'states';
import {
  JobShopQuery,
  ShedulingParameter,
  Task,
  TaskInput,
  useJobShopQuery,
} from '../../generated/graphql';

const unit = 1000 * 60 * 15;

export function SheduleOptimised({ fixed, unfixed }: { fixed: ItemState; unfixed: ItemState }) {
  const [currentDate, setCurrentStartDate] = React.useState(new Date(1996, 0, 1));
  const [viewName, setViewName] = React.useState('Week');

  const { optimizationStartDate, optimizationEndDate } = getOptimizationDate(currentDate, viewName);

  const parameter: ShedulingParameter = createShedulingParameter(
    fixed,
    unfixed,
    optimizationStartDate,
    optimizationEndDate
  );

  const { data: tasks, loading, error } = useJobShopQuery({
    variables: {
      parameter,
    },
  });

  const [editingOptions, setEditingOptions] = React.useState({
    allowAdding: true,
    allowDeleting: true,
    allowUpdating: true,
    allowDragging: true,
    allowResizing: true,
  });

  const {
    allowAdding,
    allowDeleting,
    allowUpdating,
    allowResizing,
    allowDragging,
  } = editingOptions;

  const TimeTableCell = React.useCallback(
    React.memo(({ ...restProps }) => <WeekView.TimeTableCell {...restProps} />),
    []
  );

  const CommandButton = React.useCallback(
    ({ id, ...restProps }) => {
      if (id === 'deleteButton') {
        return (
          <AppointmentTooltip.CommandButton id={id} {...restProps} disabled={!allowDeleting} />
        );
      }
      return <AppointmentTooltip.CommandButton id={id} {...restProps} />;
    },
    [allowDeleting]
  );

  const allowDrag = React.useCallback(() => allowDragging && allowUpdating, [
    allowDragging,
    allowUpdating,
  ]);
  const allowResize = React.useCallback(() => allowResizing && allowUpdating, [
    allowResizing,
    allowUpdating,
  ]);

  if (loading) return null;
  if (error) return <div>`Error! ${String(error.stack)}`</div>;

  const optimised = convertOptimisedTasksToAppointmentModels(
    tasks,
    fixed,
    unfixed,
    optimizationStartDate,
    optimizationEndDate
  );

  return (
    <>
      <Scheduler data={optimised} height={600} firstDayOfWeek={1}>
        <ViewState
          defaultCurrentDate={currentDate}
          onCurrentDateChange={setCurrentStartDate}
          onCurrentViewNameChange={setViewName}
        />

        <WeekView startDayHour={0} endDayHour={24} timeTableCellComponent={TimeTableCell} />
        <Toolbar />
        <DateNavigator />
        <TodayButton />

        <Appointments />

        <AppointmentTooltip showOpenButton showDeleteButton={allowDeleting} />
      </Scheduler>
      <div>{String(tasks)}</div>
    </>
  );
}

function getOptimizationDate(currentDate: Date, viewName: string) {
  if (viewName === 'Day') {
    return { optimizationStartDate: currentDate, optimizationEndDate: currentDate };
  }
  if (viewName === 'Week') {
    currentDate = new Date(currentDate);
    let firstDay = currentDate.getDate() - currentDate.getDay();
    const optimizationStartDate = new Date(currentDate);
    optimizationStartDate.setDate(firstDay);
    const optimizationEndDate = new Date(optimizationStartDate);
    optimizationEndDate.setDate(optimizationEndDate.getDate() + 8);
    return {
      optimizationStartDate,
      optimizationEndDate,
    };
  }
  throw new RangeError();
}

function convertDateToUTC(date: SchedulerDateTime) {
  if (typeof date === 'string' || typeof date === 'number') {
    return new Date(date);
  } else {
    return new Date(
      date.getUTCFullYear(),
      date.getUTCMonth(),
      date.getUTCDate(),
      date.getUTCHours(),
      date.getUTCMinutes(),
      date.getUTCSeconds()
    );
  }
}

function toAppointmentModel(a: AppointmentModel, date: Date): AppointmentModel {
  var duration = new Date(a.endDate!).getTime() - new Date(a.startDate).getTime();
  date = convertDateToUTC(date);
  const endDate = new Date(date.getTime() + duration);
  return { ...a, startDate: date, endDate };
}

export function createShedulingParameter(
  fixed: ItemState,
  unfixed: ItemState,
  optimizationStartDate: Date,
  optimizationEndDate: Date
): ShedulingParameter {
  const tasks: AppointmentModelAndTaskInput[] = [
    ...convertAppointmentModelToTask(fixed, optimizationStartDate, optimizationEndDate, true),
    ...convertAppointmentModelToTask(unfixed, optimizationStartDate, optimizationEndDate, false),
  ];

  console.log(tasks);

  for (const { event, task } of tasks) {
    if (!event.notes) continue;

    const param: any = yaml.load(event.notes);
    if (param.previous) {
      const prev = tasks
        .filter((e) => e.event.title === param.previous && e.event.startDate <= event.endDate!)
        .reduce((prev, current) =>
          prev.event.startDate > current.event.startDate ? prev : current
        );
      if (prev) {
        task.previous_id = prev.task.id;
      }
    }
    if (param.following) {
      const prev = tasks
        .filter((e) => e.event.title === param.following && event.startDate! <= e.event.endDate!)
        .reduce((prev, current) =>
          prev.event.startDate > current.event.startDate ? prev : current
        );
      if (prev) {
        task.following_id = prev.task.id;
      }
    }
  }

  return { tasks: tasks.map((e) => e.task) };
}

interface AppointmentModelAndTaskInput {
  event: AppointmentModel;
  task: TaskInput;
}

function convertAppointmentModelToTask(
  item: ItemState,
  start: Date,
  end: Date,
  isFixed: boolean
): AppointmentModelAndTaskInput[] {
  const data = (item.children as any) as AppointmentModel[];
  const todayOneTimeEvents = data.filter(
    (e) => !e.rRule && start < e.endDate! && e.startDate < end
  );
  const todayReccurentEvents = filterRangedReccurentEvents(data, start, end);
  const todayEvents = todayOneTimeEvents.concat(todayReccurentEvents);
  const tasks = todayEvents.map((event) => {
    const args = argsFactory(convertStartAppointmentModelForOptimiser(event, start, isFixed));
    const task: TaskInput = {
      id: calnderToTaskId(event),
      previous_id: -1,
      following_id: -1,
      resources: [0],
      ...args,
    };
    return { event: event, task: task };
  });
  return tasks;
}

function convertOptimisedTasksToAppointmentModels(
  tasks: JobShopQuery | undefined,
  fixed: ItemState,
  unfixed: ItemState,
  start: Date,
  end: Date
) {
  if (!tasks || !tasks.jobShop) return;

  const appointments: AppointmentModel[] = tasks.jobShop.map((e) => {
    const model = taskToOriginalAppointmentModel(e, fixed, unfixed);
    const startDate = convertDateOptimisedToAppointmentModel(e.start, start);
    const endDate = convertDateOptimisedToAppointmentModel(e.end, start);
    return {
      id: e.id,
      title: model?.title,
      startDate: startDate,
      endDate: endDate,
    };
  });
  return appointments;
}

const _calnderIdToTaskId = new Map<string, number>();

function convertToDuration(e: AppointmentModel): number {
  return Math.round((new Date(e.endDate!).getTime() - new Date(e.startDate).getTime()) / unit);
}

interface TaskTimeArguments {
  start: number;
  duration: number;
  duration_lower_limit: number;
  duration_upper_limit: number;
  start_range_start: number;
  start_range_end: number;
}

function argsFactory({
  start = -1,
  duration = -1,
  duration_lower_limit = -1,
  duration_upper_limit = -1,
  start_range_start = -1,
  start_range_end = -1,
}: Partial<TaskTimeArguments> = {}): TaskTimeArguments {
  return {
    start,
    duration,
    duration_lower_limit,
    duration_upper_limit,
    start_range_start,
    start_range_end,
  };
}

function convertStartAppointmentModelForOptimiser(
  task: AppointmentModel,
  optimizationStartDate: Date,
  isFixed: boolean
): Partial<TaskTimeArguments> {
  let duration = convertToDuration(task);
  let start = calcDuration(task.startDate, optimizationStartDate);
  if (!isFixed) {
    const ret: Partial<TaskTimeArguments> = {};
    if (task.notes) {
      const param: any = yaml.load(task.notes);
      if (param.duration) {
        ret.duration = Math.round((param.duration * 1000 * 60) / unit);
      }
      if (param.duration_lower_limit) {
        ret.duration_lower_limit = toTaskTick(param.duration_lower_limit);
        ret.duration_upper_limit = toTaskTick(param.duration_upper_limit);
      }
    }
    if (!task.allDay) {
      ret.start_range_start = start;
      ret.start_range_end = start + duration;
    }
    return ret;
  }

  if (start < 0) {
    duration += start;
    start = 0;
  }
  return {
    start: start,
    duration: duration,
  };
}

function toTaskTick(duration: number): number {
  return Math.round((duration * 1000 * 60) / unit);
}

function convertDateOptimisedToAppointmentModel(date: number, start: Date): Date {
  return new Date(start.getTime() + date * unit);
}

function calcDuration(date1: Date | string | number, date2: Date | string | number): number {
  return Math.round((new Date(date1).getTime() - new Date(date2).getTime()) / unit);
}

function calnderToTaskId(id: AppointmentModel) {
  var taskId = _calnderIdToTaskId.get(JSON.stringify(id));
  if (taskId !== undefined) return taskId;
  _calnderIdToTaskId.set(JSON.stringify(id), _calnderIdToTaskId.size);
  return _calnderIdToTaskId.size - 1;
}

function taskIdToCalnderId(id: number) {
  for (const [key, value] of _calnderIdToTaskId.entries()) {
    if (value === id) return key;
  }
}

function findAppointmentModel(id: string, item: ItemState) {
  const data = (item.children as any) as AppointmentModel[];
  return data.find((e) => e.id === id);
}

function assertIsDefined<T>(val: T): asserts val is NonNullable<T> {
  if (val === undefined || val === null) {
    throw new Error(`Expected 'val' to be defined, but received ${val}`);
  }
}
function taskToOriginalAppointmentModel(task: Task, fixed: ItemState, unfixed: ItemState) {
  const id = taskIdToCalnderId(task.id);
  assertIsDefined(id);
  const model_id = JSON.parse(id)['id'];
  return findAppointmentModel(model_id, fixed) || findAppointmentModel(model_id, unfixed);
}

function formatDate(date: SchedulerDateTime) {
  if (typeof date === 'string' || typeof date === 'number') {
    return String(date);
  } else {
    const y = date.getFullYear();
    const m = ('0' + (date.getMonth() + 1)).slice(-2);
    const d = ('0' + date.getDate()).slice(-2);
    const t = ('0' + date.getHours()).slice(-2);
    const min = ('0' + date.getMinutes()).slice(-2);
    const s = ('0' + date.getSeconds()).slice(-2);
    return [y, m, d, 'T', t, min, s, 'Z'].join('');
  }
}

export function convertLocalDateToUTCIgnoringTimezone(date: Date) {
  const timestamp = Date.UTC(
    date.getFullYear(),
    date.getMonth() - 1,
    date.getDate(),
    date.getHours(),
    date.getMinutes(),
    date.getSeconds(),
    date.getMilliseconds()
  );

  return new Date(timestamp);
}

export function convertUTCToLocalDateIgnoringTimezone(utcDate: Date) {
  return new Date(
    utcDate.getUTCFullYear(),
    utcDate.getUTCMonth(),
    utcDate.getUTCDate(),
    utcDate.getUTCHours(),
    utcDate.getUTCMinutes(),
    utcDate.getUTCSeconds(),
    utcDate.getUTCMilliseconds()
  );
}

function filterRangedReccurentEvents(data: AppointmentModel[], start: Date, end: Date) {
  return data.flatMap((e) => {
    if (!e.rRule) return [];

    let rRuleStr = 'dtstart:' + formatDate(e.startDate) + '\n' + e.rRule;
    if (e.exDate) {
      rRuleStr += '\n' + 'exdate:';
      const datas: string[] = new Array(0);
      for (const date of e.exDate.split(',')) {
        datas.push(
          formatDate(
            convertLocalDateToUTCIgnoringTimezone(
              new Date(
                Number(date.substring(0, 4)),
                Number(date.substring(4, 6)),
                Number(date.substring(6, 8)),
                Number(date.substring(9, 11)),
                Number(date.substring(11, 13)),
                Number(date.substring(13, 15))
              )
            )
          )
        );
      }
      rRuleStr += datas.join(',');
    }
    const rule = rrulestr(rRuleStr);

    console.log(rule.options);
    console.log(rule.toString());

    console.log(rule.between(start, end));

    return rule.between(start, end).map((d) => toAppointmentModel(e, d));
  });
}
