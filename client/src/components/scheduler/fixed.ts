export const appointments = [
  {
    id: 0,
    title: 'sleep',
    startDate: '1995-12-30T16:30:00.000Z',
    endDate: '1995-12-31T01:00:00.000Z',
    allDay: false,
    rRule: 'RRULE:INTERVAL=1;FREQ=DAILY;COUNT=30',
  },
];
