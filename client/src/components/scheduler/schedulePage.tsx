import Paper from '@material-ui/core/Paper';
import * as React from 'react';

import { AppointmentModel } from '@devexpress/dx-react-scheduler';

import { ProjectChildProps } from 'hooks/hook';
import { ItemProps, RootProps } from 'hooks/schedule';
import { useProps } from 'hooks/schedulePage';
import { ItemState } from 'states';
import { appointments as flexibleAppointments } from './flexible';
import { Schedule } from './schedule';
import { SheduleOptimised } from './schedule-optimised';

function toAppointmentModel(a: AppointmentModel, date: Date): AppointmentModel {
  var duration = (a.endDate as Date).getTime() - (a.startDate as Date).getTime();
  const endDate = new Date(a.endDate!);
  endDate.setMilliseconds(endDate.getMilliseconds() + duration);
  return { ...a, startDate: date, endDate };
}

export function SchedulePage(props: ProjectChildProps) {
  const data = (props.doc.items[0].children as any) as AppointmentModel[];
  const container = props.doc.items.find((e) => e.text.toString() === 'fixed')!;
  // const [data, setData] = React.useState<AppointmentModel[]>(appointments);
  const [flexibleData, setFlexibleData] = React.useState<AppointmentModel[]>(flexibleAppointments);
  const [editingOptions, setEditingOptions] = React.useState({
    allowAdding: true,
    allowDeleting: true,
    allowUpdating: true,
    allowDragging: true,
    allowResizing: true,
  });

  const {
    allowAdding,
    allowDeleting,
    allowUpdating,
    allowResizing,
    allowDragging,
  } = editingOptions;

  console.log(JSON.stringify(data));
  const fixedProps = createProps(props, 'fixed');
  const unfixedProps = createProps(props, 'unfixed');

  const [actionsFixed] = useProps(fixedProps);
  const [actionsUnfixed] = useProps(unfixedProps);

  const onClickDeleteAllEvents = () => {
    actionsFixed.onClickDeleteAllEvents();
    actionsUnfixed.onClickDeleteAllEvents();
  };

  return (
    <>
      <Paper>
        <button onClick={onClickDeleteAllEvents} title="delete all events">
          delete all events
        </button>
        <div style={{ display: 'grid', gridTemplateColumns: '1fr 1fr' }}>
          <Schedule {...fixedProps} />
          <Schedule {...unfixedProps} />
          {/* Optimized <Schedule container={[]} />
          <Schedule container={[]} /> */}
        </div>
        <SheduleOptimised fixed={fixedProps.item} unfixed={unfixedProps.item} />
      </Paper>
    </>
  );
}

function createProps(props: ProjectChildProps, title: string): ItemProps {
  const index = props.doc.items.findIndex((e: ItemState) => e.text.toString() === title);
  const item = props.doc.items[index] || new ItemState(title, [new ItemState()]);
  const path = [index];

  const rootProps: RootProps = {
    rootItem: item,
    ...props,
  };
  return { rootProps, item, path };
}
