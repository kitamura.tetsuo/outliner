import * as React from 'react';

import {
  AppointmentModel,
  EditingState,
  IntegratedEditing,
  ViewState,
} from '@devexpress/dx-react-scheduler';
import {
  AppointmentForm,
  Appointments,
  AppointmentTooltip,
  DateNavigator,
  DragDropProvider,
  Scheduler,
  TodayButton,
  Toolbar,
  WeekView,
} from '@devexpress/dx-react-scheduler-material-ui';
import { ItemProps, useItem } from 'hooks/schedule';

const currentDate = '1996-01-01';

export function Schedule(props: ItemProps) {
  const [item, isAppointmentBeingCreated, addedAppointment, actions] = useItem(props);

  const data = (props.item.children as any) as AppointmentModel[];
  const [editingOptions, setEditingOptions] = React.useState({
    allowAdding: true,
    allowDeleting: true,
    allowUpdating: true,
    allowDragging: true,
    allowResizing: true,
  });

  const {
    allowAdding,
    allowDeleting,
    allowUpdating,
    allowResizing,
    allowDragging,
  } = editingOptions;

  const TimeTableCell = React.useCallback(
    React.memo(({ ...restProps }) => <WeekView.TimeTableCell {...restProps} />),
    []
  );

  const CommandButton = React.useCallback(
    ({ id, ...restProps }) => {
      if (id === 'deleteButton') {
        return (
          <AppointmentTooltip.CommandButton id={id} {...restProps} disabled={!allowDeleting} />
        );
      }
      return <AppointmentTooltip.CommandButton id={id} {...restProps} />;
    },
    [allowDeleting]
  );

  const allowDrag = React.useCallback(() => allowDragging && allowUpdating, [
    allowDragging,
    allowUpdating,
  ]);
  const allowResize = React.useCallback(() => allowResizing && allowUpdating, [
    allowResizing,
    allowUpdating,
  ]);

  console.log(JSON.stringify(data));

  return (
    <>
      <Scheduler data={data} height={600} firstDayOfWeek={1}>
        <ViewState defaultCurrentDate={currentDate} />
        <EditingState
          onCommitChanges={actions.onCommitChanges}
          addedAppointment={addedAppointment}
          onAddedAppointmentChange={actions.onAddedAppointmentChange}
        />
        <IntegratedEditing />
        <WeekView timeTableCellComponent={TimeTableCell} />
        {/* <WeekView startDayHour={6} endDayHour={19} timeTableCellComponent={TimeTableCell} /> */}
        <Toolbar />
        <DateNavigator />
        <TodayButton />
        <Appointments />
        <AppointmentTooltip showOpenButton showDeleteButton={allowDeleting} />
        <AppointmentForm
          commandButtonComponent={CommandButton}
          readOnly={isAppointmentBeingCreated ? false : !allowUpdating}
        />
        <DragDropProvider allowDrag={allowDrag} allowResize={allowResize} />
      </Scheduler>
    </>
  );
}
