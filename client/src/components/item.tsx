import { Frontend } from 'automerge';
import { createRef, useMemo } from 'react';

import { ItemProps, useItem } from '../hooks/hook';
import styles from './item.module.css';

export function Item(props: ItemProps) {
  const [item, text, actions] = useItem(props);

  const ref = useMemo(() => {
    const ref = createRef<HTMLInputElement>();
    props.rootProps.pathToRefs.set(item.pathKey, ref);
    return ref;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.path]);
  // props.parent?.itemRefs.set(getObjectId( props.item), inputElement)

  // const itemRefs = new Map<string, React.LegacyRef<HTMLInputElement> | undefined>();

  const list = props.item.children.map((item, index) => {
    return (
      <li key={Frontend.getObjectId(item)}>
        <Item {...props} item={item} path={props.path.concat(index)} isTop={false} />
      </li>
    );
  });

  return (
    <>
      <input
        type="text"
        ref={ref}
        value={text}
        onChange={actions.onChange}
        onCompositionEnd={actions.onCompositionEnd}
        onKeyDown={actions.onKeyDown}
        onMouseUp={actions.onMouseUp}
      />
      {list.length ? <ul className={styles.app}>{list}</ul> : undefined}
    </>
  );
}
