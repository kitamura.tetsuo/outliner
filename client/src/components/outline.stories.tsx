import React, { ComponentProps } from "react";

import { Story, Meta } from "@storybook/react";

import { Outline } from "./outline";

//👇 This default export determines where your story goes in the story list
export default {
  title: "outline",
  component: Outline,
} as Meta;

//👇 We create a “template” of how args map to rendering
const Template: Story<ComponentProps<typeof Outline>> = (args) => (
  <Outline {...args} />
);

export const FirstStory = Template.bind({});
FirstStory.args = {
  /*👇 The args you need here will depend on your component */
};
