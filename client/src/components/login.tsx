import { auth, provider } from './firebase';
import { signInWithRedirect, User, signInWithEmailAndPassword } from 'firebase/auth';
import { useHistory } from 'react-router-dom';

export type LoginProps = {
  user?: User;
  setUser: (value: User) => void;
};

export function Login(props: LoginProps) {
  const history = useHistory();

  if (props.user) {
    history.push('/');
  }

  const handleLogin = () => {
    signInWithRedirect(auth, provider);
  };

  const handleLoginWithPassword = () => {
    signInWithEmailAndPassword(auth, 'kitamura.tetsuo.test@gmail.com', 'kb4pazA5QnJjBDjDuQxJ');
  };

  return (
    <div>
      <h1>login</h1>
      <button onClick={handleLogin}>Google login</button>
      <button onClick={handleLoginWithPassword}>login with Test Account</button>
    </div>
  );
}
