import { useEffect, useMemo, useRef, useState } from 'react';
import { Frontend } from 'automerge';

import { Item } from './item';
import { ProjectChildProps, RootProps } from '../hooks/hook';
import { TextSelection } from '../state/textSelection';
import { ItemState } from '../states';

export type OutlineProps = {
  /**
   * Is this the principal call to action on the page?
   */
  projectChildProps: ProjectChildProps;
  item?: ItemState;
  index: number;
};

export function Outline(props: OutlineProps) {
  const path: number[] = [];
  const textSelections: TextSelection[] = useMemo(() => [], []);

  const inputSelectionPosition = useRef<number>(0);

  const pathToRefs = useMemo(() => new Map<string, React.RefObject<HTMLInputElement>>(), []);
  const [focused, setFocused] = useState<string | null>(null);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  useEffect(() => {
    if (focused) {
      const element = pathToRefs.get(focused)?.current;
      if (!element) return;
      element.focus();
      element.setSelectionRange(inputSelectionPosition.current, inputSelectionPosition.current);
      setFocused(null);
    }
  });

  const item = props.item || new ItemState(props.projectChildProps.title, [new ItemState()]);
  const key = props.item ? Frontend.getObjectId(props.item) : 'new Item';

  const rootProps: RootProps = {
    pathToRefs,
    setFocused,
    inputSelectionPosition,
    textSelections,
    rootItem: item,
    ...props.projectChildProps,
  };

  return (
    <Item
      rootProps={rootProps}
      item={item}
      path={path.concat(props.index)}
      isTop={true}
      key={key}
    />
  );
}
