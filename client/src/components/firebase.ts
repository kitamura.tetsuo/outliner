import { initializeApp } from 'firebase/app';
import { getAuth, GoogleAuthProvider } from 'firebase/auth';

const firebaseConfig = {
  apiKey: 'AIzaSyC2FLThXmw4Xf_-c0FGHOtTLsGfQSTMqEY',
  authDomain: 'outliner-b9da8.firebaseapp.com',
  databaseURL: 'https://outliner-b9da8.firebaseio.com',
  projectId: 'outliner-b9da8',
  storageBucket: 'outliner-b9da8.appspot.com',
  messagingSenderId: '687163115479',
  appId: '1:687163115479:web:b17cc12b04778cee',
};
initializeApp(firebaseConfig);

// firebase.auth().onAuthStateChanged((user) => {
//   if (user) {
//     userManager.user = user;
//     actionInWebworker.setUserEmail(user.email);
//     userManager.setAuthorized(true);
//   }
// });

export const auth = getAuth();
export const provider = new GoogleAuthProvider();
