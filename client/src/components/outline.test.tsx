import jest from 'jest-mock';
import { BrowserRouter as Router, Switch, Route, useParams, Link } from 'react-router-dom';
import { ProjectState, ItemState } from '../states';
import { render, screen } from '@testing-library/react';
import { Frontend } from 'automerge';
import { tearDownForTest, registerAutomergeManagerInClientWebworker } from '../hooks/doc';
import { createBrowserHistory } from 'history';
import { createMemoryHistory } from 'history';

import { ErrorBoundary } from './errorBoundary';
import { Outline } from './outline';
import userEvent from '@testing-library/user-event';
import { PageType, Project } from './project';
import { remoteAutomergeManagerInClientWebworker } from '../web-worker/remote';

const project = new ProjectState();
project.name = 'test';
const firstTop = new ItemState('first_top');
project.items.push(firstTop);
firstTop.children.push(
  new ItemState('first child'),
  new ItemState('second child'),
  new ItemState('third child')
);
firstTop.children[0].children.push(new ItemState('first grandchild'));
firstTop.children[1].children.push(new ItemState('second grandchild'));
firstTop.children[2].children.push(new ItemState('third grandchild'));
project.items.push(new ItemState('second top'));
project.items.push(new ItemState('third top'));

describe('Outline', () => {
  beforeAll(() => {
    registerAutomergeManagerInClientWebworker(remoteAutomergeManagerInClientWebworker);
  });

  beforeEach(() => {
    // initializeCityDatabase();
  });

  afterEach(() => {
    tearDownForTest();
    // clearCityDatabase();
  });

  test.only('renders Outline component', async () => {
    renderDefault();
    expect(screen.getByDisplayValue('first_top')).toBeInTheDocument();

    await userEvent.type(screen.getByDisplayValue('second child'), ' enter text');
  });

  test('2nd renders Outline component', async () => {
    renderDefault();
    expect(screen.getByDisplayValue('first_top')).toBeInTheDocument();

    await userEvent.type(screen.getByDisplayValue('second child'), ' enter text');
  });

  test('move focus renders Outline component', async () => {
    renderDefault();
    expect(screen.getByDisplayValue('first_top')).toBeInTheDocument();

    await userEvent.type(screen.getByDisplayValue('second child'), '{arrowdown} enter text');
  });

  test('move second child item with move down block option', async () => {
    renderDefault();
    expect(screen.getByDisplayValue('first_top')).toBeInTheDocument();

    await userEvent.click(screen.getByDisplayValue('second child'));
    await userEvent.type(screen.getByDisplayValue('second child'), '{alt}{arrowdown}{/alt}');

    const expectedOrder = [
      'first_top',
      'first child',
      'first grandchild',
      'third child',
      'third grandchild',
      'second child',
      'second grandchild',
    ];
    const elements = screen.queryAllByDisplayValue(/.*/);
    expect(Array.from(elements).map((el) => (el as HTMLInputElement).value)).toMatchObject(
      expectedOrder
    );
  });

  test('move second child item with move down line option', async () => {
    renderDefault();
    expect(screen.getByDisplayValue('first_top')).toBeInTheDocument();

    await userEvent.click(screen.getByDisplayValue('second child'));
    await userEvent.type(screen.getByDisplayValue('second child'), '{ctrl}{arrowdown}{/ctrl}');

    const expectedOrder = [
      'first_top',
      'first child',
      'first grandchild',
      'second grandchild',
      'second child',
      'third child',
      'third grandchild',
    ];
    const elements = screen.queryAllByDisplayValue(/.*/);
    expect(Array.from(elements).map((el) => (el as HTMLInputElement).value)).toMatchObject(
      expectedOrder
    );
  });

  test('move second child item with move up block option', async () => {
    renderDefault();
    expect(screen.getByDisplayValue('first_top')).toBeInTheDocument();

    await userEvent.click(screen.getByDisplayValue('second child'));
    await userEvent.type(screen.getByDisplayValue('second child'), '{alt}{arrowup}{/alt}');

    const expectedOrder = [
      'first_top',
      'second child',
      'second grandchild',
      'first child',
      'first grandchild',
      'third child',
      'third grandchild',
    ];
    const elements = screen.queryAllByDisplayValue(/.*/);
    expect(Array.from(elements).map((el) => (el as HTMLInputElement).value)).toMatchObject(
      expectedOrder
    );
  });

  // test.only('move second child item with move up line option', async () => {
  //   renderDefault();
  //   expect(screen.getByDisplayValue('first_top')).toBeInTheDocument();

  //   await userEvent.click(screen.getByDisplayValue('second child'));
  //   await userEvent.type(screen.getByDisplayValue('second child'), '{ctrl}{arrowup}{/ctrl}');

  //   const expectedOrder = [
  //     'first_top',
  //     'first child',
  //     'second child',
  //     'first grandchild',
  //     'second grandchild',
  //     'third child',
  //     'third grandchild',
  //   ];
  //   const elements = screen.queryAllByDisplayValue(/.*/);
  //   expect(Array.from(elements).map((el) => (el as HTMLInputElement).value)).toMatchObject(
  //     expectedOrder
  //   );
  // });
});

function renderDefault() {
  const index = 0;
  const history = createMemoryHistory();
  history.push('/test/first');
  render(
    <Router>
      <Project type={PageType.Outline} project={project} index={index} projectId={project.name} />
    </Router>
  );
}
