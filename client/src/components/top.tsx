import { FreezeObject, getObjectId } from 'automerge';
import { ItemState } from '../states';

import { ProjectChildProps } from '../hooks/hook';

export function Top(props: ProjectChildProps) {
  if (!props.doc.items) return <></>;

  const list = props.doc.items.map((item) => {
    return (
      <li key={getObjectId(item)}>
        <a href={toUrl(props, item)}>{item.text.toString()}</a>
      </li>
    );
  });

  return (
    <>
      <ul>{list}</ul>
    </>
  );
}
function toUrl(props: ProjectChildProps, item: FreezeObject<ItemState>): string | undefined {
  return `${encodeURIComponent(props.doc.name)}/${encodeURIComponent(item.text.toString())}`;
}
