import { remoteAutomergeManagerInClientWebworker } from '../web-worker/remote';
import { useInput } from '../hooks/hook';

export function CreateProject() {
  const name = useInput('');

  const handleLogin = () => {
    remoteAutomergeManagerInClientWebworker?.createNewDocInServer(name.value);
  };

  return (
    <>
      <label>
        Name:
        <input type="text" {...name} />
      </label>
      <input type="button" onClick={handleLogin} />
    </>
  );
}
