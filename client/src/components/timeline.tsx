import FullCalendar, { EventInput } from '@fullcalendar/react';
import timeGridPlugin from '@fullcalendar/timegrid';
import resourceTimelinePlugin from '@fullcalendar/resource-timeline';

import { useState } from 'react';
import SplitPane from 'react-split-pane';
import { ItemState } from '../states';

import { Outline, OutlineProps } from './outline';
import { ProjectChildProps } from '../hooks/hook';

function getTasks(tasks: ItemState[], item: ItemState) {
  if (item.isTask) {
    tasks.push(item);
  }
  item.children.forEach((e) => getTasks(tasks, e));
}

// export function Timeline(props: OutlineProps) {
//   const [events, setEvents] = useState<EventInput[]>([]);

//   const tasks: ItemState[] = [];
//   props.projectChildProps.doc.items.forEach((e) => getTasks(tasks, e));

//   return (
//     <SplitPane split="vertical" minSize={50} defaultSize={250}>
//       <FullCalendar plugins={[timeGridPlugin]} initialView="timeGridDay" events={events} />
//       <Outline {...props} />
//     </SplitPane>
//   );
// }

// export function Timeline() {
export function Timeline(props: ProjectChildProps) {
  const [events, setEvents] = useState<EventInput[]>([
    {
      id: '1',
      resourceId: 'a',
      start: '2021-09-14',
      end: '2021-09-16',
      title: 'event 1',
      editable: true,
    },
    {
      id: '2',
      resourceId: 'b',
      start: '2021-09-16',
      end: '2021-09-22',
      title: 'event 2',
      editable: true,
    },
  ]);
  const [resources, setResources] = useState<EventInput[]>([
    { id: 'a', title: 'FullCalendar' },
    { id: 'b', title: 'Laravel' },
  ]);

  // const tasks: ItemState[] = [];
  // props.projectChildProps.doc.items.forEach((e) => getTasks(tasks, e));

  return (
    <SplitPane split="vertical" minSize={50} defaultSize={250}>
      <FullCalendar
        schedulerLicenseKey={'CC-Attribution-NonCommercial-NoDerivatives'}
        plugins={[resourceTimelinePlugin]}
        initialView="resourceTimeline"
        resources={resources}
        events={events}
        headerToolbar={{
          start: 'prev,next', // will normally be on the left. if RTL, will be on the right
          center: 'title',
          end: 'today resourceTimelineDay,resourceTimelineWeek,resourceTimelineMonth', // will normally be on the right. if RTL, will be on the left
        }}
        editable={true}

        // duration= { days: 3 }
      />
      {/* <FullCalendar plugins={[timeGridPlugin]} initialView="timeGridDay" events={events} /> */}
      {/* <Outline {...props} /> */}
    </SplitPane>
  );

  // return (
  //   <FullCalendar
  //
  //     plugins={[resourceTimelinePlugin]}
  //     initialView="resourceTimeline"
  //     events={events}
  //     resources={resources}
  //   />
  // );

  // return (
  //   <SplitPane split="vertical" minSize={50} defaultSize={250}>
  //     <FullCalendar
  //       plugins={[resourceTimelinePlugin]}
  //       initialView="resourceTimeline"
  //       events={events}
  //       resources={resources}
  //     />
  //     <Outline {...props} />
  //   </SplitPane>
  // );
}
