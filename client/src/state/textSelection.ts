export class TextSelection {
  public startItemPath: number[] = [];
  public startIndex: number = 0;
  public endItemPath: number[] = [];
  public endIndex: number = 0;
  // public startLine: TextSelectionLine = new TextSelectionLine();
  // public middleLine: TextSelectionLine;
  // public endLine: TextSelectionLine;

  constructor(
    startItemPath: number[],
    startIndex: number,
    endItemPath: number[],
    endIndex: number
  ) {
    this.startItemPath = startItemPath;
    this.startIndex = startIndex;
    this.endItemPath = endItemPath;
    this.endIndex = endIndex;
  }
  // getStartItem(outline: Outline): ItemHolderCustomElement {
  //   return outline.rootCustomElement.getHolerByPath(this.startItemPath);
  // }

  // getEndItem(outline: Outline): ItemHolderCustomElement {
  //   return outline.rootCustomElement.getHolerByPath(this.endItemPath);
  // }

  // @modelAction
  // set(
  //   outline: Outline,
  //   startItem: [ItemHolderCustomElement | number[], number],
  //   endItem: [ItemHolderCustomElement | number[], number]
  // ) {
  //   if (
  //     (startItem[0] == endItem[0] ||
  //       (Array.isArray(startItem[0]) &&
  //         Array.isArray(endItem[0]) &&
  //         deepEqual(startItem[0], endItem[0]))) &&
  //     startItem[1] > endItem[1]
  //   )
  //     [startItem[1], endItem[1]] = [endItem[1], startItem[1]];
  //   this.setStartWithoutUpdate(...startItem);
  //   this.setEnd(outline, ...endItem);
  // }

  // setStartWithoutUpdate(
  //   startItem: ItemHolderCustomElement | number[],
  //   startIndex: number
  // ) {
  //   if (!startItem) return;

  //   if (startItem instanceof ItemHolderCustomElement) {
  //     startItem.itemCustomElement.isSelected = true;
  //     this.startItemPath = startItem.getPath();
  //     this.startIndex = Math.max(0, startIndex);
  //   } else {
  //     this.startItemPath = startItem;
  //     this.startIndex = startIndex;
  //   }
  // }

  // reorder(outline: Outline) {
  //   if (!this.startItemPath || !this.endItemPath) return;
  //   if (deepEqual(this.startItemPath, this.endItemPath)) {
  //     if (this.startIndex > this.endIndex)
  //       [this.startIndex, this.endIndex] = [this.endIndex, this.startIndex];
  //   } else {
  //     const start = outline.rootCustomElement.getHolerIndexByPath(
  //       this.startItemPath
  //     );
  //     const end = outline.rootCustomElement.getHolerIndexByPath(
  //       this.endItemPath
  //     );
  //     let needReorder = false;
  //     for (let index = 0; index < Math.min(start.length, end.length); index++) {
  //       if (start[index] > end[index]) {
  //         needReorder = true;
  //         break;
  //       }
  //       if (start[index] < end[index]) {
  //         return;
  //       }
  //     }
  //     if (!needReorder && start.length < end.length) return;
  //     [this.startItemPath, this.endItemPath] = [
  //       this.endItemPath.slice(),
  //       this.startItemPath.slice(),
  //     ];
  //     [this.startIndex, this.endIndex] = [this.endIndex, this.startIndex];
  //   }
  // }

  // setStart(
  //   outline: Outline,
  //   startItem: ItemHolderCustomElement | number[],
  //   startIndex: number
  // ) {
  //   this.setStartWithoutUpdate(startItem, startIndex);
  //   this.reorder(outline);
  //   this.updatePosition(outline);
  // }

  // setEnd(
  //   outline: Outline,
  //   endItem: ItemHolderCustomElement | number[],
  //   endIndex: number
  // ) {
  //   if (!endItem) return;

  //   if (endItem instanceof ItemHolderCustomElement) {
  //     endItem.itemCustomElement.isSelected = true;
  //     this.endItemPath = endItem.getPath();
  //     this.endIndex = Math.min(endItem.itemState.text.length, endIndex);
  //   } else {
  //     this.endItemPath = endItem;
  //     this.endIndex = endIndex;
  //   }
  //   this.reorder(outline);
  //   this.updatePosition(outline);
  // }

  // arrangeItemPath(startItem: ItemState, endItem?: ItemState) {
  //   if (startItem) {
  //     const path = startItem.getIDPath();
  //     for (let i = 0; i < this.startItemPath.length; i++) {
  //       const e = this.startItemPath[i];
  //       let find: number;
  //       if ((find = path.indexOf(e)) > -1) {
  //         this.startItemPath.splice(i, this.startItemPath.length - i);
  //         this.startItemPath.push(...path.slice(find));
  //         break;
  //       }
  //     }
  //   }
  //   if (endItem) {
  //     const path = endItem.getIDPath();
  //     for (let i = 0; i < this.endItemPath.length; i++) {
  //       const e = this.endItemPath[i];
  //       let find: number;
  //       if ((find = path.indexOf(e)) > -1) {
  //         this.endItemPath.splice(i, this.endItemPath.length - i);
  //         this.endItemPath.push(...path.slice(find));
  //         break;
  //       }
  //     }
  //   }
  // }

  // updatePosition(outline: Outline) {
  //   if (!this.endItemPath || !this.startItemPath) return;

  //   const offset = outline.element.getBoundingClientRect();
  //   const start = this.getRect(
  //     outline.rootCustomElement.getHolerByPath(this.startItemPath),
  //     this.startIndex
  //   );
  //   if (!start) return;
  //   const end = this.getRect(
  //     outline.rootCustomElement.getHolerByPath(this.endItemPath),
  //     this.endIndex
  //   );
  //   if (!end) return;

  //   this.startLine = new TextSelectionLine();
  //   this.endLine = undefined;
  //   this.middleLine = undefined;

  //   this.startLine.left = `${start.left - offset.left}px`;
  //   this.startLine.top = `${start.top - offset.top}px`;
  //   this.startLine.height = `${start.height}px`;

  //   if (start.top == end.top) {
  //     this.startLine.width = `${end.left - start.left || 3}px`;
  //   } else {
  //     this.startLine.width = `10000px`;
  //     this.endLine = new TextSelectionLine();
  //     this.endLine.left = `0px`;
  //     this.endLine.top = `${end.top - offset.top}px`;
  //     this.endLine.width = `${end.left - offset.left}px`;
  //     this.endLine.height = `${end.height}px`;
  //     if (end.top - start.bottom > 1) {
  //       this.middleLine = new TextSelectionLine();
  //       this.middleLine.left = `0px`;
  //       this.middleLine.top = `${start.bottom - offset.top}px`;
  //       this.middleLine.width = `10000px`;
  //       this.middleLine.height = `${end.top - start.bottom}px`;
  //     }
  //   }
  // }

  // private getRect(item: ItemHolderCustomElement, index: number): DOMRect {
  //   if (!item) return;

  //   if (item.itemCustomElement.textbox.firstChild?.nodeType == Node.TEXT_NODE) {
  //     let range = new Range();
  //     let child = item.itemCustomElement.textbox.firstChild;
  //     range.setStart(child, Math.min(index, child.textContent.length));
  //     range.setEnd(child, Math.min(index, child.textContent.length));
  //     const end = range.getBoundingClientRect();
  //     return end;
  //   } else {
  //     const end = item.itemCustomElement.textbox.getBoundingClientRect();
  //     return end;
  //   }
  // }
}

// export class TextSelectionLine {
//   public left: string;
//   public top: string;
//   public width: string;
//   public height: string;
//   public backgroundColor = "#E5EBF1";
//   public position = "absolute";
// }

// 特定のアイテムを追跡し続けるために、パスで管理する事にする。
// パスは他のアイテムに変化があっても、アイテムを追跡できる。
// aurelia がインスタンスを再作成しても追跡できる。
// インスタンスID では特定のアイテムを追跡できない。Hokder は aurelia に任せるので、コントロールしてない。
