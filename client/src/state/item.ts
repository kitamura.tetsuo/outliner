import { Doc } from 'automerge';

import { Err, Ok, Result } from 'result';
import { getLast } from '../util/util';
// import { ItemProps } from '../hooks/hook';
import { ProjectState, ItemState } from '../states';

export interface ItemProps {
  rootProps: RootProps;
  item: ItemState;
  path: number[];
}

export interface RootProps {
  doc: Doc<ProjectState>;
  rootItem: ItemState;
}

export class Item {
  project: Doc<ProjectState>;
  path: number[];
  private _state: ItemState | undefined;
  private rootItem: ItemState | undefined;

  constructor(project: Doc<ProjectState>, path: number[], state?: ItemState, rootItem?: ItemState) {
    this.project = project;
    this.path = path;
    this._state = state;
    this.rootItem = rootItem;
  }

  static from(props: ItemProps) {
    return new Item(props.rootProps.doc, props.path, props.item, props.rootProps.rootItem);
  }

  toChange(project: Doc<ProjectState>) {
    return new Item(project, this.path, undefined, this.rootItem);
  }

  get hasNextChild() {
    const parent = this.getParent();
    if (parent.isErr()) return false;
    return parent.value.lengthOfChildren - 1 !== this.index;
  }

  get hasChild() {
    return this.lengthOfChildren > 0;
  }

  get indentDepth(): number {
    return this.path.length;
  }

  get pathKey(): string {
    return this.path.join('/');
  }

  get topIndex(): number {
    return this.path[0];
  }

  getStateToChange(project: Doc<ProjectState>) {
    if (this.path[0] === -1) {
      if (!this.rootItem) {
        throw new Error('rootItem is undefined');
      }
      const length = project.items.push(this.rootItem);
      this.path[0] = length - 1;
    }
    return getObject(project, this.path);
  }

  get state() {
    if (this._state === undefined) {
      this._state = getObject(this.project, this.path);
    }
    return this._state;
  }

  getChild(index: number): Item {
    return new Item(this.project, this.path.concat(index));
  }

  getParent(): Result<Item, unknown> {
    if (this.path.length >= 2) {
      return Ok(new Item(this.project, this.path.slice(0, -1)));
    }
    return Err();
  }

  getByIndentDepth(indentDepth: number): Result<Item, unknown> {
    if (this.indentDepth < indentDepth) {
      return Err();
    }
    if (this.indentDepth === indentDepth) {
      return Ok(this);
    }
    const parent = this.getParent();
    if (parent.isErr()) return Err();
    return parent.value.getByIndentDepth(indentDepth);
  }

  getGrandParent(): Result<Item, unknown> {
    if (this.path.length >= 3) {
      return Ok(new Item(this.project, this.path.slice(0, -2)));
    }
    return Err();
  }

  get children() {
    return this.state.children.map((e, i) => this.getChild(i));
  }

  get lengthOfChildren() {
    return this.state.children.length;
  }

  get index() {
    return this.path[this.path.length - 1];
  }

  get isRootItem() {
    return this.path.length <= 1;
  }

  equal(item: Item) {
    return (
      this.path.length === item.path.length &&
      this.path.every((value, index) => value === item.path[index])
    );
  }

  getSibling(offset: number): Item {
    return new Item(this.project, this.path.slice(0, -1).concat(this.index + offset));
  }

  getPrev(): Result<Item, unknown> {
    const parent = this.getParent();
    if (this.index) {
      if (parent.isErr()) return Err();
      const prevOfSiblings = parent.value.children[this.index - 1];
      return prevOfSiblings.getLastDescendant();
    } else if (parent.isOk()) {
      return Ok(parent.value);
    }

    return Err();
  }

  getLastDescendant(): Result<Item, unknown> {
    if (!this.lengthOfChildren) return Ok(this);

    const [last] = getLast(this.children);
    return last.getLastDescendant();
  }

  getNext(ignoreChildren?: boolean): Result<Item, unknown> {
    if (!ignoreChildren) {
      if (this.lengthOfChildren > 0) {
        return Ok(this.getChild(0));
      }
      // if (this.visibleSearchResults?.length)
      //   if (
      //     (ret = this.children_id
      //       .get(this.visibleSearchResults[0].current)
      //       .getVisibleChild())
      //   )
      //     return ret;
    }

    const parent = this.getParent();
    if (parent.isErr()) return Err();

    if (this.hasNextChild) {
      return Ok(this.getSibling(1));
    }

    return parent.value.getNext(true);
  }
}

export function getObject(project: ProjectState, props: ItemProps): ItemState;
export function getObject(project: ProjectState, path: number[]): ItemState;
export function getObject(project: ProjectState, x: ItemProps | number[]): ItemState {
  if (isItemProps(x)) {
    return getObjectSub(project, x.path);
  }
  return getObjectSub(project, x);
}

function isItemProps(arg: any): arg is ItemProps {
  return arg.parent !== undefined;
}

function getObjectSub(project: ProjectState, path: number[]) {
  let item = project.items[path[0]];
  for (let index = 1; index < path.length; index++) {
    item = item.children[path[index]];
  }
  return item;
}
