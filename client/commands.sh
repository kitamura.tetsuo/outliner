# generate types from schema.graphql in server
# https://github.com/apollographql/apollo-tooling/issues/2415

npx apollo@2.20.0 codegen:generate --addTypename
