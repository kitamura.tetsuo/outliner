mv /workspace/client/node_modules/automerge/backend/encoding.js /workspace/client/node_modules/automerge/backend/encoding.js_bak
cat <<EOF | tee /workspace/client/node_modules/automerge/backend/encoding.js
if (typeof TextEncoder === 'undefined') {
  global.TextEncoder = require('util').TextEncoder;
  global.TextDecoder = require('util').TextDecoder;
}

EOF

cat /workspace/client/node_modules/automerge/backend/encoding.js_bak | tee -a /workspace/client/node_modules/automerge/backend/encoding.js
rm /workspace/client/node_modules/automerge/backend/encoding.js_bak
